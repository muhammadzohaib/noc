let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
    'https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js',
    'https://unpkg.com/vue',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/datatables.net/js/jquery.dataTables.js',
    'node_modules/datatables.net-buttons/js/dataTables.buttons.js',
    'node_modules/datatables.net-buttons/js/buttons.html5.js',
    'public/vendor/jsvalidation/js/jsvalidation.js',
    'node_modules/icheck/icheck.js',
    'node_modules/select2/dist/js/select2.js',
    'resources/assets/dist/js/app.min.js',
],  'public/js/all.js');

mix.styles([
    'resources/assets/dist/css/style.css',
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'resources/assets/dist/css/common.css',
    'node_modules/font-awesome/css/font-awesome.css',
    'node_modules/ionicons/css/ionicons.css',
    'node_modules/datatables/media/css/jquery.dataTables.css',
    'node_modules/datatables.net-buttons-dt/css/buttons.dataTables.min.css',
     'node_modules/select2/dist/css/select2.css',
    'resources/assets/dist/css/font-family.css',
    'vendor/jeroennoten/laravel-adminlte/resources/assets/dist/css/skins/skin-green.css',
    'vendor/jeroennoten/laravel-adminlte/resources/assets/dist/css/AdminLTE.css'
   ], 'public/css/all.css');

mix.copyDirectory('node_modules/font-awesome/fonts', 'public/fonts');
mix.copy('node_modules/ionicons/fonts/ionicons.ttf', 'public/fonts/ionicons.ttf');
mix.copy('node_modules/ionicons/fonts/ionicons.woff', 'public/fonts/ionicons.woff');
mix.copy('node_modules/bootstrap/fonts/glyphicons-halflings-regular.ttf', 'public/fonts/glyphicons-halflings-regular.ttf');
mix.copy('node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff', 'public/fonts/glyphicons-halflings-regular.woff');
mix.copy('node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff2', 'public/fonts/glyphicons-halflings-regular.woff2');
mix.copy('node_modules/datatables/media/images/sort_asc.png', 'public/images/sort_asc.png');
mix.copy('node_modules/datatables/media/images/sort_both.png', 'public/images/sort_both.png');
