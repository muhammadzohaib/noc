@extends('partials.master')
@section('title','Update Branch')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>UPDATE BRANCH</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{action('BranchesController@update',$branches->id)}}">
                                {{csrf_field()}}
                                <div class="col-lg-6 col-md-6 col-sm-12  col-xs-12 ">

                                    <input name="_method" type="hidden" value="PATCH">
                                    <div class="form-group">
                                        <label for="branch_code">Branch Code:</label>
                                        <input tabindex="1" type="text" class="form-control" name="branch_code"
                                               value="{{$branches->branch_code}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="branch_name">Branch Name:</label>
                                        <input tabindex="3" type="text" class="form-control" name="branch_name"
                                               value="{{$branches->branch_name}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Address:</label>
                                        <input tabindex="5" type="text" class="form-control" name="address"
                                               value="{{$branches->address}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="offcity">Off City:</label>
                                        {{ Form::select('offcity', array('0' => 'NO', '1' => 'YES'), $branches->offcity , ['class' => 'form-control', 'tabindex'=>"7"]) }}
                                    </div>

                                    <div class="form-group">
                                        <label for="ip">IP:</label>
                                        <input tabindex="9" type="text" class="form-control" name="ip"
                                               value="{{$branches->ip}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="it_center">IT Center:</label>
                                        <input tabindex="11" type="text" class="form-control" name="it_center"
                                               value="{{$branches->it_center}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="lhr_tunnel">LHR Tunnel:</label>
                                        <input tabindex="13" type="text" class="form-control" name="lhr_tunnel"
                                               value="{{$branches->lhr_tunnel}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="bvpn_1_no">BVPN 1 #:</label>
                                        <input tabindex="15" type="text" class="form-control" name="bvpn_1_no"
                                               value="{{$branches->bvpn_1_no}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="bvpn_1_pw">BVPN 1 PW:</label>
                                        <input tabindex="17" type="text" class="form-control" name="bvpn_1_pw"
                                               value="{{$branches->bvpn_1_pw}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="bvpn_2_username">BVPN 2 Username:</label>
                                        <input tabindex="19" type="text" class="form-control" name="bvpn_2_username"
                                               value="{{$branches->bvpn_2_username}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="evo_esn">EVO ESN:</label>
                                        <input tabindex="21" type="text" class="form-control" name="evo_esn"
                                               value="{{$branches->evo_esn}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="ospf_area">OSPF AREA:</label>
                                        <input tabindex="23" type="text" class="form-control" name="ospf_area"
                                               value="{{$branches->ospf_area}}">
                                    </div>


                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12  col-xs-12 ">
                                    <div class="form-group">
                                        <label for="customer_id">Customer:</label>
                                        {!! Form::select('customer_id', $customers , $branches->customer_id, ['class' => 'form-control','tabindex'=>"2",'placeholder' => 'Please Select...']) !!}
                                    </div>

                                    <div class="form-group">
                                        <label for="region_id">Region:</label>
                                        {!! Form::select('region_id', $regions , $branches->region_id , ['class' => 'form-control', 'tabindex'=>"4",'placeholder' => 'Please Select...']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="atm">Atm:</label>
                                        {{ Form::select('atm', array('0' => 'NO', '1' => 'YES'), $branches->atm , ['class' => 'form-control', 'tabindex'=>"6"]) }}

                                    </div>

                                    <div class="form-group">
                                        <label for="loop_back_ip">Loop Back IP Address:</label>
                                        <input tabindex="8" type="text" class="form-control" name="loop_back_ip"
                                               value="{{$branches->loop_back_ip}}">
                                    </div>


                                    <div class="form-group">
                                        <label for="khi_tunnel">IT KHI Tunnel:</label>
                                        <input tabindex="10" type="text" class="form-control" name="khi_tunnel"
                                               value="{{$branches->khi_tunnel}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="dxx_info">DXX Info:</label>
                                        <input tabindex="12" type="text" class="form-control" name="dxx_info"
                                               value="{{$branches->dxx_info}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="bvpn_1_username">BVPN 1 Username:</label>
                                        <input tabindex="14" type="text" class="form-control" name="bvpn_1_username"
                                               value="{{$branches->bvpn_1_username}}">
                                    </div>


                                    <div class="form-group">
                                        <label for="bvpn_2_no">BVPN 2 #:</label>
                                        <input tabindex="16" type="text" class="form-control" name="bvpn_2_no"
                                               value="{{$branches->bvpn_2_no}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="bvpn_2_pw">BVPN 2 PW:</label>
                                        <input tabindex="18" type="text" class="form-control" name="bvpn_2_pw"
                                               value="{{$branches->bvpn_2_pw}}">
                                    </div>


                                    <div class="form-group">
                                        <label for="evo_mdn">EVO MDN:</label>
                                        <input tabindex="20" type="text" class="form-control" name="evo_mdn"
                                               value="{{$branches->evo_mdn}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="branch_power_status">Branch Power Status:</label>
                                        {!! Form::select('branch_power_status', array('1' => 'ON', '0' => 'OFF'), $branches->branch_power_status , ['class' => 'form-control', 'tabindex'=>"22"]) !!}
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Update
                                            Branch
                                        </button>
                                        <a style="margin-right:2px; float:right;" href="{{ url('branches') }}"
                                           class="btn btn-danger">Cancel
                                        </a>
                                    </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('js')
    {!! $validator !!}
@endsection