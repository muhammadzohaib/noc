@extends('partials.master')
@section('title','branches')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>BRANCHES<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                       href="{{ url('branches/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add Branch</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='branches' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Branch Code</th>
                                        <th>Branch Name</th>
                                        <th>Region Name</th>
                                        <th>Off City</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


@endsection
@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        oTable = $('#branches').DataTable({

            "processing": true,
            "serverSide": true,
            "type": JSON,
            "ajax": "{{ route('getbranches') }}",

            "columns": [
                {data: 'id', name: 'id'},
                {data: 'branch_code', name: 'branch_code'},
                {data: 'branch_name', name: 'branch_name'},
                {data: 'region_name', name: 'region_name'},
                {data: 'offcity', name: 'offcity'},
                {data: 'action', name: 'action', "orderable": false}

            ]
        });
    });
</script>

@endsection
