@extends('partials.master')
@section('title','Create Contact')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>CREATE BRANCH CONTACT</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{url('branchcontacts')}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">

                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">

                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Contact:</label>
                                        <input type="text" class="form-control" name="contact">
                                    </div>
                                    <div class="form-group">
                                        <label for="extension">Extension:</label>
                                        <input type="number" class="form-control" name="extension">
                                    </div>
                                    <div class="form-group">
                                        <label for="comment">Comment:</label>
                                        <input type="text" class="form-control" name="comment">
                                    </div>
                                    <div class="form-group">
                                        <label for="branch_id">Branch:</label>
                                        {!! Form::select('branch_id', $branches, null, ['class' => 'form-control','placeholder' => 'Please Select...']) !!}
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Add
                                            Contact
                                        </button>
                                        <a style="margin-right:2px; float:right;" href="{{ url('branchcontacts') }}"
                                           class="btn btn-danger">Cancel
                                        </a>
                                    </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('js')
    {!! $validator !!}
@endsection