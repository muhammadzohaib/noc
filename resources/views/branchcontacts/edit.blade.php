@extends('partials.master')
@section('title','Update Contact')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4><b>UPDATE BRANCH CONTACT</b></h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post"
                                  action="{{action('Branch_ContactsController@update',$branchcontacts->id)}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">

                                    <input name="_method" type="hidden" value="PATCH">


                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="name"
                                               value="{{$branchcontacts->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="email" class="form-control" name="email"
                                               value="{{$branchcontacts->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="contact">Contact:</label>
                                        <input type="text" class="form-control" name="contact"
                                               value="{{$branchcontacts->contact}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="extension">Extension:</label>
                                        <input type="number" class="form-control" name="extension"
                                               value="{{$branchcontacts->extension}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="comment">Comment:</label>
                                        <input type="text" class="form-control" name="comment"
                                               value="{{$branchcontacts->comment}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="branch_id">Branch:</label>
                                        {!! Form::select('branch_id', $branches, $branchcontacts->branch_id, ['class' => 'form-control','placeholder' => 'Please Select...']) !!}
                                    </div>
                                     <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Update
                                            Contact
                                        </button>
                                        <a style="margin-right:2px; float:right;" href="{{ url('branchcontacts') }}"
                                           class="btn btn-danger">Cancel
                                        </a>
                                    </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('js')
    {!! $validator !!}
@endsection