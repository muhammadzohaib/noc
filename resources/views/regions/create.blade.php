@extends('partials.master')
@section('title','Create-Region')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>CREATE REGION</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{url('regions')}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    <div class="form-group">
                                        <label for="region_code">Region Code:</label>
                                        <input type="text" class="form-control" name="region_code">
                                    </div>

                                    <div class="form-group">
                                        <label for="region_name">Region Name:</label>
                                        <input type="text" class="form-control" name="region_name">
                                    </div>

                                    <div class="form-group">
                                        <label for="manager_name">Manager Name:</label>
                                        <input type="text" class="form-control" name="manager_name">
                                    </div>

                                    <div class="form-group">
                                        <label for="manager_no">Manager No:</label>
                                        <input type="text" class="form-control" name="manager_no">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Add Region
                                        </button>
                                        <a style="margin-right:2px; float:right;" href="{{ url('regions') }}"
                                           class="btn btn-danger">Cancel
                                        </a>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
@section('js')
    {!! $validator !!}
@endsection