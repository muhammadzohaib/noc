@extends('partials.master')
@section('title','Regions')
@section('content')
    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Regions<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                      href="{{ url('/regions/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Create Region</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='regions' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Region Code</th>
                                        <th>Region Name</th>
                                        <th>Manager Name</th>
                                        <th>Manager No.</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($regions as $region)
                                        <tr>
                                            <td>{{$region['id']}}</td>
                                            <td>{{$region['region_code']}}</td>
                                            <td>{{$region['region_name']}}</td>
                                            <td>{{$region['manager_name']}}</td>
                                            <td>{{$region['manager_no']}}</td>
                                            <td><a href="{{action('RegionsController@edit', $region['id'])}}"
                                                   class="btn btn-warning">Edit</a></td>
                                            <td>
                                                <form action="{{action('RegionsController@destroy', $region['id'])}}"
                                                      method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection


@push('css')
