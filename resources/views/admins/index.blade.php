@extends('partials.master')

@section('title', 'Dashboard')

@section('content')
    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Dashboard</h4>
                        <p>Welcome to this beautiful admin panel.</p>
                        {{--<h5>Welcome Manager</h5>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@push('css')