@extends('partials.master')
@section('title','TT Types')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>TROUBLE TICKET TYPES<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                                   href="{{ url('servicetypes/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add New</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='servicetypes' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($servicetypes as $service)
                                        <tr>
                                            <td>{{$service['id']}}</td>
                                            <td>{{$service['title']}}</td>
                                            <td>{{$service['description']}}</td>
                                            <td class="edit-btn"><a
                                                        href="{{action('ServiceTypeController@edit', $service['id'])}}"
                                                        class="btn btn-warning">Edit</a></td>

                                            <td>
                                                <form action="{{action('ServiceTypeController@destroy', $service['id'])}}"
                                                      method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
