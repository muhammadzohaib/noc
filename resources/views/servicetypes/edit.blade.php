@extends('partials.master')
@section('title','Update TT Type')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>UPDATE TROUBLE TICKET TYPE</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{action('ServiceTypeController@update',$servicetypes->id)}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">

                                    <input name="_method" type="hidden" value="PATCH">


                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    <div class="form-group">
                                        <label for="title">Title:</label>
                                        <input type="text" class="form-control" name="title"
                                               value="{{$servicetypes->title}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description:</label>
                                        <input type="text" class="form-control" name="description"
                                               value="{{$servicetypes->description}}">
                                    </div>


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Update
                                        </button>
                                        <a style="margin-right:2px; float:right;" href="{{ url('servicetypes') }}"
                                           class="btn btn-danger">Cancel
                                        </a>
                                    </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('js')
    {!! $validator !!}
@endsection