@extends('partials.master')
@section('title','Update Customer')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>Change Password</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{action('UsersController@updatepassword')}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    {{--<input name="_method" type="hidden" value="PATCH">--}}
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    <div class="form-group">
                                        <label for="alias">Old Password:</label>
                                        <input type="password" class="form-control" name="old_password"
                                               value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Password:</label>
                                        <input type="password" class="form-control" name="password" value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="name">Confirm Password:</label>
                                        <input type="password" class="form-control" name="password_confirmation" value="">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Change Password
                                        </button>
                                    </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection