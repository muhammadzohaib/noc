@extends('layouts.app')
@section('reg_login_css')
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    @yield('css')
@endsection
@section('body_class', 'register-page')
@section('body')

    <div class="register-box">
        <div class="register-logo">
            <a href="{{ url('/')}}">{!! config('noc.logo', '<b>NOC</b>CMS') !!}</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Register</p>
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                {!! csrf_field() !!}
                <div class="form-group has-feedback {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="name" type="text" name="name" class="form-control" value="{{ old('name') }}"
                           placeholder="Full Name" required autofocus>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                           placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required
                           placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                           required
                           placeholder="Confirm Password">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit"
                        class="btn btn-primary btn-block btn-flat"
                >Register
                </button>
            </form>

        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->

@endsection
