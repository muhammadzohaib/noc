@extends('partials.master')
@section('title','BranchHardwares')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>BRANCH HARDWARES<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                            href="{{ url('branchhardwares/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add Branch Hardware</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='branchehardwares' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Branch</th>
                                        <th>Hardware Name</th>
                                        <th>Hardware Brand</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                  </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#branchehardwares').DataTable({

                "processing": true,
                "serverSide": true,
                "type": JSON,
                "ajax": "{{ route('getbranchhardwares') }}",

                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'branch', name: 'branch'},
                    {data: 'hardware_name', name: 'hardware_name'},
                    {data: 'hardware_brand', name: 'hardware_brand'},
                    {data: 'action', name: 'action', "orderable": false}

                ]
            });
        });
    </script>
@endsection


