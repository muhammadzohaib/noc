@extends('partials.master')
@section('title','Update Customer')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>UPDATE SUPPORT STAFF</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{action('SupportStaffController@update',$supportstaff->id)}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">

                                    <input name="_method" type="hidden" value="PATCH">


                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    <div class="form-group">
                                        <label for="name">Name:</label>
                                        <input type="text" class="form-control" name="name"
                                               value="{{$supportstaff->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email:</label>
                                        <input type="email" class="form-control" name="email"
                                               value="{{$supportstaff->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Phone:</label>
                                        <input type="text" class="form-control" name="phone"
                                               value="{{$supportstaff->phone}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="city">City:</label>
                                        <input type="text" class="form-control" name="city"
                                               value="{{$supportstaff->city}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="address">Address:</label>
                                        <input type="text" class="form-control" name="address"
                                               value="{{$supportstaff->address}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="region_id">Region:</label>
                                        {!! Form::select('region_id', $regions , $supportstaff->region_id , ['class' => 'form-control' ,'placeholder' => 'Please Select...']) !!}
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Update
                                            Support Staff
                                        </button>
                                        <a style="margin-right:2px; float:right;" href="{{ url('supportstaff') }}"
                                           class="btn btn-danger">Cancel
                                        </a>

                                    </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('js')
    {!! $validator !!}
@endsection