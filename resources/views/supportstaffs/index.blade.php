@extends('partials.master')
@section('title','Customers')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>SUPPORT STAFF<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                            href="{{ url('supportstaff/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add Support Staff</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='supportstaff' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>City</th>
                                        <th>Region</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($supportstaff as $support)
                                        <tr>
                                            <td>{{$support['id']}}</td>
                                            <td>{{$support['name']}}</td>
                                            <td>{{$support['email']}}</td>
                                            <td>{{$support['phone']}}</td>
                                            <td>{{$support['city']}}</td>
                                            <td>{{$support->region['region_name']}}</td>
                                            <td class="edit-btn"><a
                                                        href="{{action('SupportStaffController@edit', $support['id'])}}"
                                                        class="btn btn-warning">Edit</a></td>

                                            <td>
                                                <form action="{{action('SupportStaffController@destroy', $support['id'])}}"
                                                      method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection


@push('css')
