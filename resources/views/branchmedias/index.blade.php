@extends('partials.master')
@section('title','BranchMedias')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>BRANCH MEDIAS<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                              href="{{ url('branchmedias/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add Branch Media</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='branchemedias' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Branch</th>
                                        <th>Media Name</th>
                                        <th>IP</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                   </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#branchemedias').DataTable({

                "processing": true,
                "serverSide": true,
                "type": JSON,
                "ajax": "{{ route('getbranchmedias') }}",

                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'branch', name: 'branch'},
                    {data: 'media_name', name: 'media_name'},
                    {data: 'ip', name: 'ip'},
                    {data: 'action', name: 'action', "orderable": false}

                ]
            });
        });
    </script>

@endsection

