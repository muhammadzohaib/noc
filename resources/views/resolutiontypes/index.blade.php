@extends('partials.master')
@section('title','Customers')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>RESOLUTION TYPES<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                            href="{{ url('resolutiontypes/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add Resolution Type</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='resolutiontypes' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($resolutiontypes as $resolution)
                                        <tr>
                                            <td>{{$resolution['id']}}</td>
                                            <td>{{$resolution['name']}}</td>
                                            <td>{{$resolution['description']}}</td>
                                            <td class="edit-btn"><a
                                                        href="{{action('ResolutionTypesController@edit', $resolution['id'])}}"
                                                        class="btn btn-warning">Edit</a></td>

                                            <td>
                                                <form action="{{action('ResolutionTypesController@destroy', $resolution['id'])}}"
                                                      method="post">
                                                    {{csrf_field()}}
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection


@push('css')
