<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>NOC CMS - @yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{asset('/css/all.css')}}" rel="stylesheet" type="text/css"/>

</head>
<body class="skin-green">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo"><b><i>NOC</i></b>  CMS</a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu hidden">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">4</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 4 messages</li>
                            <li>
                                <!-- inner menu: contains the messages -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <a href="#">
                                            <div class="pull-left">
                                                <!-- User Image -->
                                                <img src="{{ asset("/assets/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image"/>
                                            </div>
                                            <!-- Message title and timestamp -->
                                            <h4>
                                                Support Team
                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                            </h4>
                                            <!-- The message -->
                                            <p>Why not buy a new awesome theme?</p>
                                        </a>
                                    </li><!-- end message -->
                                </ul><!-- /.menu -->
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li><!-- /.messages-menu -->

                    <!-- Notifications Menu -->
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li>
                                <!-- Inner Menu: contains the notifications -->
                                <ul class="menu">
                                    <li><!-- start notification -->
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li><!-- end notification -->
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                    <!-- Tasks Menu -->
                    <li class="dropdown tasks-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <span class="label label-danger">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 9 tasks</li>
                            <li>
                                <!-- Inner menu: contains the tasks -->
                                <ul class="menu">
                                    <li><!-- Task item -->
                                        <a href="#">
                                            <!-- Task title and progress text -->
                                            <h3>
                                                Design some buttons
                                                <small class="pull-right">20%</small>
                                            </h3>
                                            <!-- The progress bar -->
                                            <div class="progress xs">
                                                <!-- Change the css width attribute to simulate progress -->
                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                    <span class="sr-only">20% Complete</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li><!-- end task item -->
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{ asset("/assets/dist/img/user2-160x160.jpg") }}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">Name here</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{ asset("/resources/assets/dist/img/users/default-avatar.png") }}" class="img-circle" alt="User Image" />
                                <p>
                                    role name here
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    {{--<img src="{{ asset("/assets/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />--}}
                </div>
                <div class="pull-left info">
                    <p></p>

                </div>
            </div>



            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li><h4 style="color: #fff;font-style: italic;line-height: 0px;padding-bottom: 0px;">
                        User: {{ $name }}</h4></li>
                <li>
                    <span style="color: #fff;font-size: 12px;font-style: italic;"><b>Role:&nbsp;&nbsp;</b>{{ $role }}</span>
                </li>
                <hr/>
                @if($role != "user")
                <li class="treeview">
                   <a href="#"><span>Branches</span> <i class="fa fa-angle-left pull-right"></i></a>
                   <ul class="treeview-menu">
                       <li><a href="{{ url('branches') }}"><span>Branches</span></a></li>
                       <li><a href="{{ url('branchcontacts') }}"><span>Branch Contacts</span></a></li>
                       <li><a href="{{ url('branchhardwares') }}"><span>Branch Hardwares</span></a></li>
                       <li><a href="{{ url('branchserviceproviders') }}"><span>Branch Service Providers</span></a></li>
                       <li><a href="{{ url('branchmedias') }}"><span>Branch Medias</span></a></li>
                   </ul>
               </li>

                <li><a href="{{ url('customers') }}"><span>Customers</span></a></li>
                <li><a href="{{ url('regions') }}"><span>Regions</span></a></li>
                 <li><a href="{{ url('resolutiontypes') }}"><span>Resolution Types</span></a></li>

                    <li class="treeview">
                        <a href="#"><span>Trouble Tickets</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('complaints') }}"><span>All Tickets</span></a></li>
                            <li><a href="{{ url('unresolvedtickets') }}"><span>Un Resolved</span></a></li>
                            <li><a href="{{ url('resolvedtickets') }}"><span>Resolved</span></a></li>
                        </ul>
                    </li>

                <li><a href="{{ url('servicetypes') }}"><span>Trouble Ticket Types</span></a></li>
                <li><a href="{{ url('supportstaff') }}"><span>Support Staffs</span></a></li>
                <li><a href="{{ url('userslist') }}"><span>Users</span></a></li>
                @endif
                @if($role == "user")
                    <li class="treeview">
                        <a href="#"><span>Trouble Tickets</span> <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('complaints') }}"><span>All Tickets</span></a></li>
                            <li><a href="{{ url('unresolvedtickets') }}"><span>Un Resolved</span></a></li>
                            <li><a href="{{ url('resolvedtickets') }}"><span>Resolved</span></a></li>
                        </ul>
                    </li>
                @endif
                <li class="treeview">
                    <a href="#"><span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('changepassword') }}"><span>Change Password</span></a></li>
                    </ul>
                </li>
                <li><a href="{{ url('logout') }}"><span>Logout</span></a></li>
                <!--       <li class="treeview">
                           <a href="#"><span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
                           <ul class="treeview-menu">
                               <li><a href="#">Link in level 2</a></li>
                               <li><a href="#">Link in level 2</a></li>
                           </ul>
                       </li> -->
            </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @yield('content')
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            NOC CMS
        </div>
        <!-- Default to the left -->
        <strong>Copyright © 2017-2018 <a target="_blank" href="http://www.future-technologies.com">Future
                Technologies</a>.</strong> All rights reserved.
    </footer>

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.3 -->

<!-- Bootstrap 3.3.2 JS -->


<script src="{{ asset ('/js/all.js') }}"></script>

@yield('js')
<!-- AdminLTE App -->

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>