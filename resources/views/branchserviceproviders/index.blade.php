@extends('partials.master')
@section('title','Regions')
@section('content')
    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Branch Service Providers<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                      href="{{ url('/branchserviceproviders/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Create Service Provider</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='serviceprovider' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Branch</th>
                                        <th>Code</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            console.log("sending req");
            oTable = $('#serviceprovider').DataTable({
                "processing": true,
                "serverSide": true,
                "type": JSON,
                "ajax": "{{ route('getserviceproviders') }}",
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'branch_name', name: 'branch_name'},
                    {data: 'code', name: 'code'},
                    {data: 'action', name: 'action', "orderable": false}
                ]
            });
        });
    </script>
@endsection