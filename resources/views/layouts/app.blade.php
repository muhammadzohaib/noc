<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>NOC CMS - Login</title>

    <!-- Styles -->
    @yield('reg_login_css')
    <link href="{{asset('/css/all.css')}}" rel="stylesheet" type="text/css"/>



</head>
<body class="hold-transition @yield('body_class')">
@yield('body')

<script src="{{ asset('/js/all.js') }}"></script>

@yield('js')
</body>
</html>
