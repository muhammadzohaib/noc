@extends('partials.master')

@section('title', 'Dashboard')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>DASHBOARD</h4>
                        {{--<h5>Welcome Manager</h5>--}}

                    </div>
                </div>
            </div>
        </div>
        <div class="box collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title" style="font-family: Saira;"><b>Choose Date Range</b></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                        <i class="fa fa-plus"></i></button>
                </div>
            </div>
            {{ Form::open(array('url' => '/managerdashboard')) }}
            <div class="box-body">
                {{csrf_field()}}
                <div class="col-md-2">
                {{ Form::radio('daterange', 'Today', true, array('id'=>'daterange-0')) }}
                {{ Form::label('daterange-0', 'Today') }}
                </div>
                <div class="col-md-2">
                {{ Form::radio('daterange', 'Week', false, array('id'=>'daterange-0')) }}
                {{ Form::label('daterange-0', 'Week') }}
                </div>
                    <div class="col-md-2">
                {{ Form::radio('daterange', 'Month', false, array('id'=>'daterange-0')) }}
                {{ Form::label('daterange-0', 'Month') }}
                    </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <input class="btn btn-success pull-right" type="submit" value="Apply">
            </div>
        {{ Form::close() }}
            <!-- /.box-footer-->
        </div>

        <div class="row">
            <div class="col-lg-4 col-xs-4">
                <a href="complaints">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{$totalComplaints}}</h3>

                        <p>Total Tickets</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-call"></i>
                    </div>

                </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-4">
                <a href="unresolvedtickets">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$totalNotResolvedComplaints}}</h3>

                        <p>Ticket Opened</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-call"></i>
                    </div>

                </div>
                </a>
            </div>
            <div class="col-lg-4 col-xs-4">
                <a href="resolvedtickets">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$totalResolvedComplaints}}</h3>

                        <p>Ticket Closed</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-call"></i>
                    </div>

                </div>
                </a>
            </div>

         </div>
        {{--<div class="row" style="margin-top:10px;">--}}
            {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h4>Complaints By Region</h4>--}}

                    {{--</div>--}}
                    {{--<div class="panel-body">--}}

                        {{--<div class="row">--}}
                            {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
                                {{--<table id='RegionComplaints' class="table table-responsive table-striped">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th>Name</th>--}}
                                        {{--<th>Total</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}
                                    {{--@foreach($getRegionComplaints as $getRegionComplaint)--}}
                                        {{--<tr>--}}
                                            {{--<td>{{$getRegionComplaint['region_name']}}</td>--}}
                                            {{--<td>{{$getRegionComplaint['complaints_count']}}</td>--}}
                                        {{--</tr>--}}
                                    {{--@endforeach--}}
                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="row" style="margin-top:10px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Complaints By Customers</h4>

                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='CustomerComplaints' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($getCustomerComplaints as $getCustomerComplaint)
                                        <tr>
                                            <td>{{$getCustomerComplaint['alias']}}</td>
                                            <td>{{$getCustomerComplaint['complaints_count']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:10px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Complaints By ATM</h4>

                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='CustomerComplaintsAtm' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($getCustomerAtmComplaints as $getCustomerAtmComplaint)
                                        <tr>
                                            <td>{{$getCustomerAtmComplaint['alias']}}</td>
                                            <td>{{$getCustomerAtmComplaint['complaints_count']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--  <div class="row" style="margin-top:10px;">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <h4>Complaints By Off-City</h4>

                      </div>
                      <div class="panel-body">

                          <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <table id='CustomerComplaints' class="table table-responsive table-striped">
                                      <thead>
                                      <tr>
                                          <th>Name</th>
                                          <th>Total</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                      <tr>
                                          <td>On-City</td>
                                          <td>0</td>
                                      </tr>
                                      <tr>
                                          <td>Off-City</td>
                                          <td>1</td>
                                      </tr>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
  --}}
        <div class="row" style="margin-top:10px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Complaints By Service Type</h4>

                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='ComplaintsByServiceType' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($getComplaintsByServiceTypes as $getComplaintsByServiceType)
                                        <tr>
                                            <td>{{$getComplaintsByServiceType['title']}}</td>
                                            <td>{{$getComplaintsByServiceType['complaints_count']}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
