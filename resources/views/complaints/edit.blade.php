@extends('partials.master')
@section('title','Update TT')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>UPDATE TROUBLE TICKET</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{action('ComplaintController@update',$complaints->id)}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    <input name="_method" type="hidden" value="PATCH">
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">
                                    <div class="form-group">
                                        <label for="customer_id">Customer:</label>
                                        <input type="text" class="form-control" name="customer_id" disabled=true
                                               value="{{$complaints->customer->name}}"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="servicetype_id">Complaint Type:</label>
                                        {!! Form::select('servicetype_id', $servicetypes, $complaints->servicetype_id, ['class' => 'form-control','disabled'=>true,'placeholder' => 'Please Select...']) !!}
                                    </div>

                                    <div class="form-group">
                                        <label for="branch_id">Branch:</label>
                                        <input type="text" class="form-control" name="branch_id" disabled=true
                                               value="{{$complaints->branch->branch_name}}"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="complaint_description">Complaint Notes:</label>
                                        <textarea rows="6" type="text" class="form-control"
                                                  name="complaint_description">{{$complaints->complaint_description}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="ptcl_ticket_no">Ptcl Ticket No:</label>
                                        <input type="text" class="form-control" name="ptcl_ticket_no"
                                               value="{{$complaints->ptcl_ticket_no}}"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="supportstaff_id">Support Staff:</label>
                                        {!! Form::select('supportstaff_id', $supportstaffs, $complaints->supportstaff_id, ['class' => 'form-control','placeholder' => 'Please Select...']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status:</label>
                                        {!! Form::select('status', ['Resolved' => 'Resolved', 'Not Resolved' => 'Not Resolved'], $complaints->status, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        <label for="resolutiontypes_id">Resolution Type:</label>
                                        {!! Form::select('resolutiontypes_id', $resolutiontypes, $complaints->resolutiontypes_id, ['class' => 'form-control','placeholder' => 'Please Select...']) !!}
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Resolution Notes:</label>
                                        <textarea rows="6" type="text" class="form-control"
                                                  name="description">{{$complaints->description}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        @if($complaints->status != 'Resolved')
                                            <button type="submit" class="btn btn-success" style="float: right">Update
                                            </button>
                                        @endif
                                        <a style="margin-right:2px; float:right;" href="{{ url('complaints') }}"
                                           class="btn btn-danger">Cancel
                                        </a>
                                    </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
@section('js')
    {!! $validator !!}
@endsection