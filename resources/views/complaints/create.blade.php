@extends('partials.master')
@section('title','Create TT')
@section('content')
    <div class="container">

        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class='panel panel-default'>
                    <div class="panel-heading">
                        <h4>CREATE TROUBLE TICKET</h4>
                    </div>
                    <div class="panel-body">
                        @if ($errors->any())
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div><br/>
                        @endif
                        @if (\Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        <p>{{ \Session::get('message') }}</p>
                                    </div>
                                </div>

                            </div><br/>
                        @endif


                        <div class="row">

                            <form method="post" action="{{url('complaints')}}">
                                {{csrf_field()}}
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12 ">

                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12  col-xs-12">

                                    <div class="form-group">
                                        <label for="customer_id">Customer:</label>
                                        {!! Form::select('customer_id', $customers , null, ['class' => 'form-control','id'=>"customer",'placeholder' => 'Please Select...']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="servicetype_id">Complaint Type:</label>
                                        {!! Form::select('servicetype_id', $servicetypes, null, ['class' => 'form-control','placeholder' => 'Please Select...']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="resolutiontypes_id">Resolution Type:</label>
                                        {!! Form::select('resolutiontypes_id', $resolutiontypes, null, ['class' => 'form-control','placeholder' => 'Please Select...']) !!}
                                    </div>
                                    <div class="form-group" id="branch_id">
                                        <label for="branch_id">Branch:</label>
                                        {!! Form::select('branch_id', array(), null, ['v-model'=>"selected",'placeholder' => 'Please Select...', 'class' => 'form-control','id'=>"branch",'v-on:change'=>"onChange"]) !!}
                                    </div>
                                    <div class="form-group">
                                        <label for="complaint_description">Complaint Notes:</label>
                                        <textarea rows="6" type="text" class="form-control"
                                                  name="complaint_description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="ptcl_ticket_no">Ptcl Ticket No:</label>
                                        <input type="text" class="form-control" name="ptcl_ticket_no"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="supportstaff_id">Support Staff:</label>
                                        {!! Form::select('supportstaff_id', $supportstaffs, null, ['class' => 'form-control','placeholder' => 'Please Select...']) !!}
                                    </div>
                                    <div class="form-group" id="stat">
                                        <label for="status">Status:</label>
                                        {!! Form::select('status', ['Not Resolved' => 'Not Resolved', 'Resolved' => 'Resolved'], null, ['class' => 'form-control','placeholder' => 'Please Select...'])  !!}
                                    </div>
                                    {{--<template v-if="status === 'Resolved'">--}}
                                    <div class="form-group">
                                        <label for="description">Resolution Notes:</label>
                                        <textarea rows="6" type="text" class="form-control"
                                                  name="description"></textarea>
                                    </div>
                                    {{--</template>--}}

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success" style="float: right">Add
                                            Complaint
                                        </button>
                                        @if(Auth::user()->roles()->first()->name != "user")
                                        <a style="margin-right:2px; float:right;" href="{{ url('complaints') }}"
                                           class="btn btn-danger">Cancel
                                        </a>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <br>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
@section('js')
    {!! $validator !!}
    <script>
        $(document).ready(function () {
            $('#branch').select2();
        });
        $('#customer').on('change', function(e){
            var customer_id = e.target.value;

            $.get('{{ url('getBranchesByCustomer') }}?customer_id=' + customer_id, function(data) {
                $('#branch').empty();
                $('#branch').append($("<option></option>").attr("value","").text("Please Select..."));
                $.each(data, function(index,branch){
                    $('#branch').append($("<option></option>").attr("value",branch.id).text(branch.branch_name));
                });
            });
        });
    </script>
@endsection

