@extends('partials.master')
@section('title','Resolved Tickets')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Resolved Tickes<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                         href="{{ url('complaints/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add Ticket</a></h4>
                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='complaints' class="table table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID
                                        <th>Customer</th>
                                        <th>Complaint Type</th>
                                        <th>Branch</th>
                                        <th>Support Staff</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
@section('js')

    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#complaints').DataTable({

                "processing": true,
                "serverSide": true,
                "type": JSON,
                "ajax": "{{ route('getresolvedtickets') }}",

                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'customer', name: 'customer'},
                    {data: 'service', name: 'service'},
                    {data: 'branch_name', name: 'branch_name'},
                    {data: 'staffname', name: 'staffname'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action'}

                ]
            });
        });
    </script>

@endsection
