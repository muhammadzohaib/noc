@extends('partials.master')
@section('title','TROUBLE TICKET ')
@section('content')

    <div class="container">
        <div class="row" style="margin-top:30px;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>TROUBLE TICKETS<a class='btn btn-primary' style='float:right;margin-right: 15px;'
                                              href="{{ url('complaints/create') }}">
                                <i class='glyphicon glyphicon-plus'></i> Add New</a></h4>

                    </div>
                    <div class="panel-body">
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <p>{{ \Session::get('success') }}</p>
                            </div><br/>
                        @endif

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id='complaints' class="table table-responsive table-striped" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <th>ID
                                        <th>Customer</th>
                                        <th>Ptcl Docket</th>
                                        <th>Date</th>
                                        <th>Branch</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                        <th>created_at</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
@section('js')
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src=" https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"
            type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>



    <script type="text/javascript">
        $(document).ready(function () {
            oTable = $('#complaints').DataTable({

                "processing": true,
                "serverSide": true,
                "type": JSON,
                "ajax": "{{ route('getcomplaints') }}",
                "order": [[3, "asc"]],
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ],
                "columns": [
                    {data: 'id', name: 'id'},
                    {data: 'customer', name: 'customer'},
                    {data: 'ptcl_ticket_no', name: 'ptcl_ticket_no'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'branch_name', name: 'branch_name'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', "orderable": false},
                    {data: 'created', name: 'created', "visible": false}

                ],
                "createdRow": function( row, data, dataIndex ) {
                    if (data['status'] == 'Not Resolved' && data['created'] > 24) {
                        $(row).css("background-color", "#FF6347");
                        $(row).css("color", "white");
                    }
                }
            });
        });
    </script>

@endsection
