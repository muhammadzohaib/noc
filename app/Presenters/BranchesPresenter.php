<?php

namespace App\Presenters;

use App\Transformers\BranchesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BranchesPresenter
 *
 * @package namespace App\Presenters;
 */
class BranchesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BranchesTransformer();
    }
}
