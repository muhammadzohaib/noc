<?php

namespace App\Presenters;

use App\Transformers\BranchMediasTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BranchMediasPresenter
 *
 * @package namespace App\Presenters;
 */
class BranchMediasPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BranchMediasTransformer();
    }
}
