<?php

namespace App\Presenters;

use App\Transformers\SupportstaffTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SupportstaffPresenter
 *
 * @package namespace App\Presenters;
 */
class SupportstaffPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SupportstaffTransformer();
    }
}
