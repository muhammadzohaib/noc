<?php

namespace App\Presenters;

use App\Transformers\BranchContactsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BranchContactsPresenter
 *
 * @package namespace App\Presenters;
 */
class BranchContactsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BranchContactsTransformer();
    }
}
