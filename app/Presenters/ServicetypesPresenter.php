<?php

namespace App\Presenters;

use App\Transformers\ServicetypesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ServicetypesPresenter
 *
 * @package namespace App\Presenters;
 */
class ServicetypesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ServicetypesTransformer();
    }
}
