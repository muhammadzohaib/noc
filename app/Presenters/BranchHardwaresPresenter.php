<?php

namespace App\Presenters;

use App\Transformers\BranchHardwaresTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BranchHardwaresPresenter
 *
 * @package namespace App\Presenters;
 */
class BranchHardwaresPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BranchHardwaresTransformer();
    }
}
