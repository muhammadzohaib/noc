<?php

namespace App\Presenters;

use App\Transformers\ResolutionTypesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ResolutionTypesPresenter
 *
 * @package namespace App\Presenters;
 */
class ResolutionTypesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ResolutionTypesTransformer();
    }
}
