<?php

namespace App\Presenters;

use App\Transformers\RegionsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RegionsPresenter
 *
 * @package namespace App\Presenters;
 */
class RegionsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RegionsTransformer();
    }
}
