<?php

namespace App\Presenters;

use App\Transformers\BranchServiceProviderTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BranchServiceProviderPresenter
 *
 * @package namespace App\Presenters;
 */
class BranchServiceProviderPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BranchServiceProviderTransformer();
    }
}
