<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SupportstaffPresenter
 *
 * @package namespace App\Presenters;
 */
class UserPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserPresenter();
    }
}
