<?php

namespace App\Presenters;

use App\Transformers\CustomersTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CustomersPresenter
 *
 * @package namespace App\Presenters;
 */
class CustomersPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CustomersTransformer();
    }
}
