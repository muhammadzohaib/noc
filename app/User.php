<?php

namespace App;

use App\Entities\Complaint;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements Transformable
//class User extends Model implements Transformable
 {
    use Notifiable;
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = $password;
    }

    protected $fillable = [
        'name', 'email', 'password', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',

    ];

    public function isAdmin()
    {
        return $this->admin; // this looks for an admins column in your users table
    }


    public function complaints()
    {
        return $this->hasMany('App\Entities\Complaint','user_id');
    }

    /**
     * @return array
     */
    public function transform()
    {
        // TODO: Implement transform() method.
    }


}
