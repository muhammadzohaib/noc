<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServicetypesRepository
 * @package namespace App\Repositories;
 */
interface ServicetypesRepository extends RepositoryInterface
{
    //
}
