<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BranchContactsRepository
 * @package namespace App\Repositories;
 */
interface BranchContactsRepository extends RepositoryInterface
{
    //
}
