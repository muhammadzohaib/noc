<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ResolutionTypesRepository
 * @package namespace App\Repositories;
 */
interface ResolutionTypesRepository extends RepositoryInterface
{
    //
}
