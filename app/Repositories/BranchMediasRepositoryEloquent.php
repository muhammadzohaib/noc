<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\branchMediasRepository;
use App\Entities\BranchMedias;
use App\Validators\BranchMediasValidator;

/**
 * Class BranchMediasRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BranchMediasRepositoryEloquent extends BaseRepository implements BranchMediasRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BranchMedias::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BranchMediasValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
