<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegionsRepository
 * @package namespace App\Repositories;
 */
interface RegionsRepository extends RepositoryInterface
{
    //
}
