<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BranchServiceProviderRepository
 * @package namespace App\Repositories;
 */
interface BranchServiceProviderRepository extends RepositoryInterface
{
    //
}
