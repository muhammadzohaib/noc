<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\branchHardwaresRepository;
use App\Entities\BranchHardwares;
use App\Validators\BranchHardwaresValidator;

/**
 * Class BranchHardwaresRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BranchHardwaresRepositoryEloquent extends BaseRepository implements BranchHardwaresRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BranchHardwares::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BranchHardwaresValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
