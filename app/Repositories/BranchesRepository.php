<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BranchesRepository
 * @package namespace App\Repositories;
 */
interface BranchesRepository extends RepositoryInterface
{
    //
}
