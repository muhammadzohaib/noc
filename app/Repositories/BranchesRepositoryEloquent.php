<?php

namespace App\Repositories;

use App\Entities\Branches;
use App\Validators\BranchesValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class BranchesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BranchesRepositoryEloquent extends BaseRepository implements BranchesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Branches::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return BranchesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
