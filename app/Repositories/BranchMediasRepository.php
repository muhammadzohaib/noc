<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BranchMediasRepository
 * @package namespace App\Repositories;
 */
interface BranchMediasRepository extends RepositoryInterface
{
    //
}
