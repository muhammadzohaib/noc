<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BranchHardwaresRepository
 * @package namespace App\Repositories;
 */
interface BranchHardwaresRepository extends RepositoryInterface
{
    //
}
