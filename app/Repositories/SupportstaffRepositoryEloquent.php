<?php

namespace App\Repositories;

use App\Entities\Supportstaff;
use App\Validators\SupportstaffValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SupportstaffRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SupportstaffRepositoryEloquent extends BaseRepository implements SupportstaffRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Supportstaff::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return SupportstaffValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
