<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\branchServiceProviderRepository;
use App\Entities\BranchServiceProvider;
use App\Validators\BranchServiceProviderValidator;

/**
 * Class BranchServiceProviderRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BranchServiceProviderRepositoryEloquent extends BaseRepository implements BranchServiceProviderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BranchServiceProvider::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BranchServiceProviderValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
