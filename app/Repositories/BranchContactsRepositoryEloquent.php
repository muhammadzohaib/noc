<?php

namespace App\Repositories;

use App\Entities\BranchContacts;
use App\Repositories\branch_contactsRepository;
use App\Validators\BranchContactsValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class BranchContactsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BranchContactsRepositoryEloquent extends BaseRepository implements BranchContactsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BranchContacts::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return BranchContactsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
