<?php

namespace App\Repositories;

use App\Entities\Servicetypes;
use App\Validators\ServicetypesValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ServicetypesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ServicetypesRepositoryEloquent extends BaseRepository implements ServicetypesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Servicetypes::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return ServicetypesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
