<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\resolutionTypesRepository;
use App\Entities\ResolutionTypes;
use App\Validators\ResolutionTypesValidator;

/**
 * Class ResolutionTypesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ResolutionTypesRepositoryEloquent extends BaseRepository implements ResolutionTypesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ResolutionTypes::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ResolutionTypesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
