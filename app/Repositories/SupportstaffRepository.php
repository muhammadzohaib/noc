<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SupportstaffRepository
 * @package namespace App\Repositories;
 */
interface SupportstaffRepository extends RepositoryInterface
{
    //
}
