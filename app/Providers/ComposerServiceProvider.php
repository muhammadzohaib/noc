<?php

namespace App\Providers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::composer(
            'partials.master', 'App\Http\ViewComposers\MasterComposer'
        );

        View::composer(
            ['supportstaffs.create', 'supportstaffs.edit', 'branches.create', 'branches.edit'],
            'App\Http\ViewComposers\RegionComposer'
        );

        View::composer(
            ['complaints.create', 'complaints.edit', 'branches.create', 'branches.edit'],
            'App\Http\ViewComposers\CustomerComposer'
        );

        View::composer(
            ['complaints.create', 'complaints.edit'],
            'App\Http\ViewComposers\ServicetypeComposer'
        );

        View::composer(
            ['complaints.create', 'complaints.edit', 'branchcontacts.create', 'branchcontacts.edit', 'branchmedias.create', 'branchmedias.edit', 'branchhardwares.create', 'branchhardwares.edit','branchserviceproviders.create', 'branchserviceproviders.edit'],
            'App\Http\ViewComposers\BranchComposer'
        );

        View::composer(
            ['complaints.create', 'complaints.edit'],
            'App\Http\ViewComposers\SupportstaffComposer'
        );

        View::composer(
            ['complaints.create', 'complaints.edit'],
            'App\Http\ViewComposers\ResolutionTypesComposer'
        );

        View::composer(
            ['users.create'],
            'App\Http\ViewComposers\RoleComposer'
        );
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
