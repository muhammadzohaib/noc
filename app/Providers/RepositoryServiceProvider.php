<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\CustomersRepository::class, \App\Repositories\CustomersRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RegionsRepository::class, \App\Repositories\RegionsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ServicetypesRepository::class, \App\Repositories\ServicetypesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SupportstaffRepository::class, \App\Repositories\SupportstaffRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BranchesRepository::class, \App\Repositories\BranchesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BranchContactsRepository::class, \App\Repositories\BranchContactsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ComplaintRepository::class, \App\Repositories\ComplaintRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RolesRepository::class, \App\Repositories\RolesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BranchmediaRepository::class, \App\Repositories\BranchmediaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BranchMediasRepository::class, \App\Repositories\BranchMediasRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BranchHardwaresRepository::class, \App\Repositories\BranchHardwaresRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BranchServiceProviderRepository::class, \App\Repositories\BranchServiceProviderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ResolutionTypesRepository::class, \App\Repositories\ResolutionTypesRepositoryEloquent::class);
        //:end-bindings:
    }
}
