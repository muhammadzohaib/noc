<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->register(RepositoryServiceProvider::class);
        $this->app->register(ComposerServiceProvider::class);

        $this->app->bind(\App\Services\Interfaces\CustomerInterface::class, \App\Services\CustomerService::class);
        $this->app->bind(\App\Services\Interfaces\RegionInterface::class, \App\Services\RegionService::class);
        $this->app->bind(\App\Services\Interfaces\ServiceTypesInterface::class, \App\Services\ServiceTypeService::class);
        $this->app->bind(\App\Services\Interfaces\SupportstaffInterface::class, \App\Services\SupportstaffService::class);
        $this->app->bind(\App\Services\Interfaces\BranchesInterface::class, \App\Services\BranchesService::class);
        $this->app->bind(\App\Services\Interfaces\BranchContactsInterface::class, \App\Services\BranchContactsService::class);
        $this->app->bind(\App\Services\Interfaces\ComplaintInterface::class, \App\Services\ComplaintService::class);
        $this->app->bind(\App\Services\Interfaces\RoleInterface::class, \App\Services\RoleService::class);
        $this->app->bind(\App\Services\Interfaces\DashboardInterface::class, \App\Services\DashboardService::class);
        $this->app->bind(\App\Services\Interfaces\UserInterface::class, \App\Services\UserService::class);
        $this->app->bind(\App\Services\Interfaces\DatatableInterface::class, \App\Services\DatatableService::class);
        $this->app->bind(\App\Services\Interfaces\BranchMediasInterface::class, \App\Services\BranchMediasService::class);
        $this->app->bind(\App\Services\Interfaces\BranchHardwaresInterface::class, \App\Services\BranchHardwaresService::class);
        $this->app->bind(\App\Services\Interfaces\BranchServiceProvidersInterface::class, \App\Services\BranchServiceProvidersService::class);
        $this->app->bind(\App\Services\Interfaces\ResolutionTypesInterface::class, \App\Services\ResolutionTypesService::class);
    }
}
