<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\BranchServiceProviderRepository;
use App\Services\Interfaces\BranchServiceProvidersInterface;


class BranchServiceProvidersService implements BranchServiceProvidersInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(BranchServiceProviderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllBranch_ServiceProviders()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $brannchserviceproviders = $this->repository->with('branch')->all();

            $dto->data = $brannchserviceproviders;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranch_ServiceProvidersByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $brannchserviceproviders = $this->repository->find($id);
            $dto->data = $brannchserviceproviders;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createBranch_ServiceProviders($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $brannchserviceproviders = $this->repository->create($request->all());

            $dto->message = 'Branch Service Provider created.';
            $dto->data = $brannchserviceproviders->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateBranch_ServiceProviders($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $brannchserviceproviders = $this->repository->update($request->all(), $id);
            $dto->message = 'Branch Service Provider Updated.';
            $dto->data = $brannchserviceproviders;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteBranch_ServiceProviders($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Branch Service Provider Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}