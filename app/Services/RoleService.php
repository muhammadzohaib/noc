<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\RolesRepository;
use App\Services\Interfaces\RoleInterface;


class RoleService implements RoleInterface
{
    /**
     * @var RolesRepository
     */
    protected $repository;

    public function __construct(RolesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllRoles()
    {
        $dto = new \App\Helpers\DTO;
        try {
            // die("asdf");
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $roles = $this->repository->all();

            $dto->data = $roles;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function pluckAllRoles($column, $key)
    {
        $dto = new \App\Helpers\DTO;
        try {

            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $customers = $this->repository->pluck($column, $key);
            $dto->data = $customers;

        } catch (\Exception $e) {

            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

//    public function getRegionByID($id)
//    {
//        $dto = new \App\Helpers\DTO;
//        try {
//            $region = $this->repository->find($id);
//            $dto->data = $region;
//
//        } catch (\Exception $e) {
//            $dto->error = true;
//            $dto->message = $e->getMessage();
//        }
//        return $dto;
//    }
//
//    public function createRegion($request)
//    {
//        $dto = new \App\Helpers\DTO;
//        try {
//            //dd($request->all());
//            $region = $this->repository->create($request->all());
//            $dto->message = 'Region created.';
//            $dto->data = $region->toArray();
//
//        } catch (\Exception $e) {
//            $dto->error = true;
//            $dto->message = $e->getMessage();
//        }
//        return $dto;
//    }
//
//    public function updateRegion($request, $id)
//    {
//        $dto = new \App\Helpers\DTO;
//        try {
//            //dd($id);
//            $region = $this->repository->update($request->all(), $id);
//            $dto->message = 'Region Updated.';
//            $dto->data = $region;
//
//        } catch (\Exception $e) {
//            $dto->error = true;
//            $dto->message = $e->getMessage();
//        }
//        return $dto;
//
//    }
//
//    public function deleteRegion($id)
//    {
//        $dto = new \App\Helpers\DTO;
//        try {
//            $deleted = $this->repository->delete($id);
//
//            $dto->message = 'Region Deleted.';
//            $dto->data = $deleted;
//
//        } catch (\Exception $e) {
//            $dto->error = true;
//            $dto->message = $e->getMessage();
//        }
//
//        return $dto;
//
//    }
}