<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\BranchContactsRepository;
use App\Services\Interfaces\BranchContactsInterface;


class BranchContactsService implements BranchContactsInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(BranchContactsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllBranch_Contacts()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $brannchcontacts = $this->repository->with('branch')->all();

            $dto->data = $brannchcontacts;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranch_ContactsByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $brannchcontacts = $this->repository->find($id);
            $dto->data = $brannchcontacts;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createBranch_Contacts($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $brannchcontacts = $this->repository->create($request->all());

            $dto->message = 'Branch Contact created.';
            $dto->data = $brannchcontacts->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateBranch_Contacts($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $brannchcontacts = $this->repository->update($request->all(), $id);
            $dto->message = 'Branch Contact Updated.';
            $dto->data = $brannchcontacts;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteBranch_Contact($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Branch Contact Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}