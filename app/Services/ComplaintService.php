<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\ComplaintRepository;
use App\Services\Interfaces\ComplaintInterface;
use Carbon\Carbon;


class ComplaintService implements ComplaintInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(ComplaintRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllComplaints()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $complaints = $this->repository->with(['customer', 'servicetype', 'branch', 'supportstaff'])->all();

            $dto->data = $complaints;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getComplaintsByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $complaints = $this->repository->with('branch')->with('customer')->find($id);
            $dto->data = $complaints;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createComplaint($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            $request['user_id'] = \Auth::User()->id;
            $complaints = $this->repository->create($request->all());

            $dto->message = 'Complaint created.';
            $dto->data = $complaints->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateComplaint($request, $id)
    {
        $dto = new \App\Helpers\DTO;

        try {
            if($request->status == "Resolved") {
                $request["resolved_at"] = Carbon::now()->toDateTimeString();
                $request['resolved_by'] = \Auth::User()->id;
            }
                $complaints = $this->repository->update($request->all(), $id);
                $dto->message = 'Complaint Updated.';
                $dto->data = $complaints;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteComplaint($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Complaint Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}