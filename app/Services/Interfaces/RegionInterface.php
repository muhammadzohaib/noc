<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 6:12 PM
 */

namespace App\Services\Interfaces;


interface RegionInterface
{
    public function getAllRegions();

    public function pluckAllRegions($column, $key);

    public function getRegionByID($id);

    public function createRegion($request);

    public function updateRegion($request, $id);

    public function deleteRegion($id);
}
