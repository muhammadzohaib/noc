<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 6:12 PM
 */

namespace App\Services\Interfaces;


interface DashboardInterface
{
    public function getTotalResolvedComplaints($id);

}
