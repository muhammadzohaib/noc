<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface SupportstaffInterface
{
    public function getAllSupportstaff();

    public function pluckAllSupportstaffs($column, $key);

    public function getSupportstaffByID($id);

    public function createSupportstaff($request);

    public function updateSupportstaff($request, $id);

    public function deleteSupportstaff($id);
}