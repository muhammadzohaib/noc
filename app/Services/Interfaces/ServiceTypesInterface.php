<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface ServiceTypesInterface
{
    public function getAllServiceTypes();

    public function getSevriceTypesByID($id);

    public function createServiceType($request);

    public function updateServiceType($request, $id);

    public function deleteServiceType($id);
}