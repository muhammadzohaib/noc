<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface BranchesInterface
{
    public function getAllBranches();

    public function pluckAllBranches($column, $key);

    public function getBranchesByID($id);

    public function createBranches($request);

    public function updateBranches($request, $id);

    public function deleteBranch($id);
}