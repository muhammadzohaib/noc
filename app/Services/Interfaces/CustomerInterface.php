<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface CustomerInterface
{
    public function getAllCustomers();

    public function pluckAllCustomers($column, $key);

    public function getCustomerByID($id);

    public function createCustomer($request);

    public function updateCustomer($request, $id);

    public function deleteCustomer($id);
}