<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface ComplaintInterface
{
    public function getAllComplaints();

    public function getComplaintsByID($id);

    public function createComplaint($request);

    public function updateComplaint($request, $id);

    public function deleteComplaint($id);
}