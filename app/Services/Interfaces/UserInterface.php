<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface UserInterface
{
    public function getAllUsers();

    public function getUserByID($id);

    public function createUser($request);

    public function updateUser($request, $id);

    public function deleteUser($id);

    public function updateUserPassword($request);
}