<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface DatatableInterface
{
    public function getBranchesDatatable();

    public function getComplaintsDatatable();
}