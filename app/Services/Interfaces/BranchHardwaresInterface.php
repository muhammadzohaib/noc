<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 10/3/2017
 * Time: 5:08 PM
 */

namespace App\Services\Interfaces;


interface BranchHardwaresInterface
{
    public function getAllBranch_Hardwares();

    public function getBranch_HardwaresByID($id);

    public function createBranch_Hardwares($request);

    public function updateBranch_Hardwares($request, $id);

    public function deleteBranch_Hardwares($id);
}