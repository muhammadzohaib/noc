<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 10/3/2017
 * Time: 2:32 PM
 */

namespace App\Services\Interfaces;


interface BranchMediasInterface
{
    public function getAllBranch_Medias();

    public function getBranch_MediasByID($id);

    public function createBranch_Medias($request);

    public function updateBranch_Medias($request, $id);

    public function deleteBranch_Medias($id);
}