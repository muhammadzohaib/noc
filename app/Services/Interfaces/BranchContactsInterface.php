<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 9/27/2017
 * Time: 12:30 PM
 */

namespace App\Services\Interfaces;


interface BranchContactsInterface
{
    public function getAllBranch_Contacts();

    public function getBranch_ContactsByID($id);

    public function createBranch_Contacts($request);

    public function updateBranch_Contacts($request, $id);

    public function deleteBranch_Contact($id);
}