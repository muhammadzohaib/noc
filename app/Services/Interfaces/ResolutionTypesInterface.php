<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 10/19/2017
 * Time: 6:40 PM
 */

namespace App\Services\Interfaces;


interface ResolutionTypesInterface
{
    public function getAllResolutionTypes();

    public function getResolutionTypesByID($id);

    public function createResolutionTypes($request);

    public function updateResolutionTypes($request, $id);

    public function deleteResolutionTypes($id);
}