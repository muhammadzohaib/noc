<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 10/3/2017
 * Time: 5:08 PM
 */

namespace App\Services\Interfaces;


interface BranchServiceProvidersInterface
{
    public function getAllBranch_ServiceProviders();

    public function getBranch_ServiceProvidersByID($id);

    public function createBranch_ServiceProviders($request);

    public function updateBranch_ServiceProviders($request, $id);

    public function deleteBranch_ServiceProviders($id);
}