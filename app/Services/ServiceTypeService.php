<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\ServicetypesRepository;
use App\Services\Interfaces\ServiceTypesInterface;


class ServiceTypeService implements ServiceTypesInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(ServicetypesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllServiceTypes()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $servicetypes = $this->repository->all();

            $dto->data = $servicetypes;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function pluckAllServiceTypes($column, $key)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $servicetypes = $this->repository->pluck($column, $key);

            $dto->data = $servicetypes;

        } catch (\Exception $e) {

            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getSevriceTypesByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $servicetypes = $this->repository->find($id);
            $dto->data = $servicetypes;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createServiceType($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $servicetypes = $this->repository->create($request->all());

            $dto->message = 'Service Type created.';
            $dto->data = $servicetypes->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateServiceType($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $servicetypes = $this->repository->update($request->all(), $id);
            $dto->message = 'Service Type Updated.';
            $dto->data = $servicetypes;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteServiceType($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Service Type Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}