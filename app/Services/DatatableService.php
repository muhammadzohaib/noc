<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\BranchContactsRepository;
use App\Repositories\BranchesRepository;
use App\Repositories\BranchHardwaresRepository;
use App\Repositories\BranchMediasRepository;
use App\Repositories\BranchServiceProviderRepository;
use App\Repositories\ComplaintRepository;
use App\Repositories\UserRepository;
use App\Services\Interfaces\DatatableInterface;
use Carbon\Carbon;
use DataTables;

class DatatableService implements DatatableInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;
    protected $comrepository;
    protected $branchhardwarerepository;
    protected $branchmediarepository;
    protected $branchcontactrepository;
    protected $userrepository;
    protected $serviceproviderrepository;

    public function __construct(BranchesRepository $repository, ComplaintRepository $comrepository, BranchHardwaresRepository $branchhardwarerepository, BranchMediasRepository $branchmediarepository, BranchContactsRepository $branchcontactrepository, UserRepository $userrepository,BranchServiceProviderRepository $serviceproviderrepository)
    {
        $this->repository = $repository;
        $this->comrepository = $comrepository;
        $this->branchhardwarerepository = $branchhardwarerepository;
        $this->branchmediarepository = $branchmediarepository;
        $this->branchcontactrepository = $branchcontactrepository;
        $this->userrepository = $userrepository;
        $this->serviceproviderrepository = $serviceproviderrepository;
    }

    public function getBranchesDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $branches = $this->repository->with('region')->all();

            $dto->data = DataTables::of($branches)->addColumn('region_name', function ($branches) {
                return $branches->region->region_name;
            })->addColumn('action', function ($branches) {
                return '<a href="' . route("branches.edit", $branches->id) . '" class="btn btn-warning"> Edit</a>
                       <form style="display: inline" action="' . action('BranchesController@destroy', $branches['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

            })->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getComplaintsDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $complaints = $this->comrepository->with(['customer', 'servicetype', 'branch', 'supportstaff'])->all();

            $dto->data = DataTables::of($complaints)->addColumn('customer', function ($complaints) {
                return $complaints->customer->name;
            })
                ->addColumn('service', function ($complaints) {
                    return $complaints->servicetype->title;
                })
//                ->addColumn('region_name', function ($complaints) {
//                    return $complaints->region->region_name;
//                })
                ->addColumn('branch_name', function ($complaints) {
                    return $complaints->branch->branch_name;
                })
                ->addColumn('staffname', function ($complaints) {
                    return $complaints->supportstaff->name;
                })
                ->addColumn('action', function ($complaints) {
                    if ($complaints->status != 'Resolved') {
                        return '<a href="' . route("complaints.edit", $complaints->id) . '" class="btn btn-warning"> Edit</a>';
                    } else {
                        return '<a href="' . route("complaints.edit", $complaints->id) . '" class="btn btn-success"> View</a>';
                    }

//                       <form style="display: inline" action="' . action('ComplaintController@destroy', $complaints['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

                })
                ->addColumn('created', function ($complaints) {
                    return Carbon::now()->diffInHours($complaints->created_at);
                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;
    }

    public function getUnResolvedComplaintsDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $complaints = $this->comrepository->with(['customer', 'servicetype', 'branch', 'supportstaff'])->all()->where('status', 'Not Resolved');

            $dto->data = DataTables::of($complaints)->addColumn('customer', function ($complaints) {
                return $complaints->customer->name;
            })
                ->addColumn('service', function ($complaints) {
                    return $complaints->servicetype->title;
                })
//                ->addColumn('region_name', function ($complaints) {
//                    return $complaints->region->region_name;
//                })
                ->addColumn('branch_name', function ($complaints) {
                    return $complaints->branch->branch_name;
                })
                ->addColumn('staffname', function ($complaints) {
                    return $complaints->supportstaff->name;
                })
                ->addColumn('action', function ($complaints) {
                    return '<a href="' . route("complaints.edit", $complaints->id) . '" class="btn btn-warning"> Edit</a>';
//                       <form style="display: inline" action="' . action('ComplaintController@destroy', $complaints['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

                })
                ->addColumn('created_at', function ($complaints) {
                    return Carbon::now()->diffInHours($complaints->created_at);
                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;
    }

    public function getResolvedComplaintsDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $complaints = $this->comrepository->with(['customer', 'servicetype', 'branch', 'supportstaff'])->all()->where('status', 'Resolved');


            $dto->data = DataTables::of($complaints)->addColumn('customer', function ($complaints) {
                return $complaints->customer->name;
            })
                ->addColumn('service', function ($complaints) {
                    return $complaints->servicetype->title;
                })
//                ->addColumn('region_name', function ($complaints) {
//                    return $complaints->region->region_name;
//                })
                ->addColumn('branch_name', function ($complaints) {
                    return $complaints->branch->branch_name;
                })
                ->addColumn('staffname', function ($complaints) {
                    return $complaints->supportstaff->name;
                })
                ->addColumn('action', function ($complaints) {
                    return '<a href="' . route("complaints.edit", $complaints->id) . '" class="btn btn-success"> View</a>';

                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;
    }

    public function getBranchHardwaresDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $hardwares = $this->branchhardwarerepository->with('branch')->all();

            // dd($complaints);

            $dto->data = DataTables::of($hardwares)->addColumn('branch', function ($hardwares) {
                return $hardwares->branch->branch_name;
            })
               ->addColumn('action', function ($hardwares) {
                   return '<a href="' . route("branchhardwares.edit", $hardwares->id) . '" class="btn btn-warning"> Edit</a>
                       <form style="display: inline" action="' . action('Branch_HardwaresController@destroy', $hardwares['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranchMediasDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $medias = $this->branchmediarepository->with('branch')->all();

            // dd($complaints);

            $dto->data = DataTables::of($medias)->addColumn('branch', function ($medias) {
                return $medias->branch->branch_name;
            })
                ->addColumn('action', function ($medias) {
                    return '<a href="' . route("branchmedias.edit", $medias->id) . '" class="btn btn-warning"> Edit</a>
                       <form style="display: inline" action="' . action('Branch_MediasController@destroy', $medias['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranchContactsDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $contacts = $this->branchcontactrepository->with('branch')->all();

            // dd($complaints);

            $dto->data = DataTables::of($contacts)->addColumn('branch', function ($contacts) {
                return $contacts->branch->branch_name;
            })
                ->addColumn('action', function ($contacts) {
                    return '<a href="' . route("branchcontacts.edit", $contacts->id) . '" class="btn btn-warning"> Edit</a>
                       <form style="display: inline" action="' . action('Branch_ContactsController@destroy', $contacts['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getUsersDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $users = $this->userrepository->all();

            // dd($complaints);

            $dto->data = DataTables::of($users)->addColumn('action', function ($users) {
                return '<a href="' . route("users.edit", $users->id) . '" class="btn btn-warning"> Edit</a>
                       <form style="display: inline" action="' . action('UsersController@destroy', $users['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranchServiceProvidersDatatable()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $serviceprovider = $this->serviceproviderrepository->with('branch')->all();

            $dto->data = DataTables::of($serviceprovider)->addColumn('branch_name', function ($serviceprovider) {
                return $serviceprovider->branch->branch_name;
            })
                ->addColumn('action', function ($serviceprovider) {
                    return '<a href="' . route("branchserviceproviders.edit", $serviceprovider->id) . '" class="btn btn-warning"> Edit</a>
                       <form style="display: inline" action="' . action('Branch_ServiceProvidersController@destroy', $serviceprovider['id']) . '" method="post">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE"><button type="submit" class="btn btn-danger"> Delete</button></form>';

                })
                ->make(true);
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

}