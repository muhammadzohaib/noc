<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\CustomersRepository;
use App\Services\Interfaces\CustomerInterface;


class CustomerService implements CustomerInterface
{
    /**
     * @var CustomersRepository asfdasfd
     */
    protected $repository;

    public function __construct(CustomersRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllCustomers()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $customers = $this->repository->all();

            $dto->data = $customers;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function pluckAllCustomers($column, $key)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $customers = $this->repository->pluck($column, $key);
             $dto->data = $customers;

        } catch (\Exception $e) {

            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }
    public function getCustomerByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $customer = $this->repository->find($id);
            $dto->data = $customer;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createCustomer($request)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $customer = $this->repository->create($request->all());
            $dto->message = 'Customer created.';
            $dto->data = $customer->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateCustomer($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $customer = $this->repository->update($request->all(), $id);
            $dto->message = 'Customer Updated.';
            $dto->data = $customer;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteCustomer($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Customer Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}