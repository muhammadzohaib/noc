<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\UserRepository;
use App\Services\Interfaces\UserInterface;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\User;


class UserService implements UserInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllUsers()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $users = $this->repository->all();
            $dto->data = $users;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }


    public function getUserByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $users = $this->repository->find($id);

            $dto->data = $users;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createUser($request)
    {
//        TODO: create user from repository
        $dto = new \App\Helpers\DTO;
        try {
             User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ])->roles()->attach($request->role_id);
            $dto->message = "User Created Successfully";
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateUser($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $users = $this->repository->update($request->all(), $id);
            $dto->message = 'User Updated.';
            $dto->data = $users;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteUser($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'User Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }

    public function updateUserPassword($request)
    {
        $dto = new \App\Helpers\DTO;
        try {

            $user = Auth::user();
            $curPassword = $request["old_password"];
            $newPassword = $request['password'];
            if (Hash::check($curPassword, $user->password)) {
                $user->password = Hash::make($newPassword);
                $user->save();
                $dto->message = 'User Password Updated.';
            }
            else{
                $dto->error = true;
                $dto->message = 'Invalid Old Password';
            }

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }
}