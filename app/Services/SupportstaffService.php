<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\SupportstaffRepository;
use App\Services\Interfaces\SupportstaffInterface;


class SupportstaffService implements SupportstaffInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(SupportstaffRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllSupportstaff()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $supportstaff = $this->repository->with('region')->all();
            $dto->data = $supportstaff;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function pluckAllSupportstaffs($column, $key)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $supportstaffs = $this->repository->pluck($column, $key);

            $dto->data = $supportstaffs;

        } catch (\Exception $e) {

            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getSupportstaffByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $supportstaff = $this->repository->find($id);

            $dto->data = $supportstaff;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createSupportstaff($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $supportstaff = $this->repository->create($request->all());

            $dto->message = 'Support Staff created.';
            $dto->data = $supportstaff->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateSupportstaff($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $supportstaff = $this->repository->update($request->all(), $id);
            $dto->message = 'Support Staff Updated.';
            $dto->data = $supportstaff;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteSupportstaff($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Support Staff Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}