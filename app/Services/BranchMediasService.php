<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\BranchMediasRepository;
use App\Services\Interfaces\BranchMediasInterface;


class BranchMediasService implements BranchMediasInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(BranchMediasRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllBranch_Medias()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $brannchmedias = $this->repository->with('branch')->all();

            $dto->data = $brannchmedias;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranch_MediasByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $brannchmedias = $this->repository->find($id);
            $dto->data = $brannchmedias;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createBranch_Medias($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $brannchmedias = $this->repository->create($request->all());

            $dto->message = 'Branch Contact created.';
            $dto->data = $brannchmedias->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateBranch_Medias($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $brannchmedias = $this->repository->update($request->all(), $id);
            $dto->message = 'Branch Contact Updated.';
            $dto->data = $brannchmedias;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteBranch_Medias($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Branch Contact Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}