<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Criteria\BranchCriteria;
use App\Repositories\BranchesRepository;
use App\Services\Interfaces\BranchesInterface;


class BranchesService implements BranchesInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(BranchesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllBranches()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $branches = $this->repository->with('region')->all();

            $dto->data = $branches;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function pluckAllBranches($column, $key)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $customers = $this->repository->pluck($column, $key);
          //  dd($customers);
            $dto->data = $customers;

        } catch (\Exception $e) {

            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranchesByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $branches = $this->repository->find($id);
            $dto->data = $branches;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createBranches($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $branches = $this->repository->create($request->all());

            $dto->message = 'Branch created.';
            $dto->data = $branches->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateBranches($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $branches = $this->repository->update($request->all(), $id);
            $dto->message = 'Branch Updated.';
            $dto->data = $branches;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteBranch($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Branch Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }

    public function getBranchesByCustomer()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $branches =  $this->repository->getByCriteria(new BranchCriteria());
            $dto->data = $branches;
        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }
}