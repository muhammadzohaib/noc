<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\BranchHardwaresRepository;
use App\Services\Interfaces\BranchHardwaresInterface;


class BranchHardwaresService implements BranchHardwaresInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(BranchHardwaresRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllBranch_Hardwares()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $brannchhardwares = $this->repository->with('branch')->all();

            $dto->data = $brannchhardwares;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getBranch_HardwaresByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $brannchhardwares = $this->repository->find($id);
            $dto->data = $brannchhardwares;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createBranch_Hardwares($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $brannchhardwares = $this->repository->create($request->all());

            $dto->message = 'Branch Hardware created.';
            $dto->data = $brannchhardwares->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateBranch_Hardwares($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $brannchhardwares = $this->repository->update($request->all(), $id);
            $dto->message = 'Branch Hardware Updated.';
            $dto->data = $brannchhardwares;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteBranch_Hardwares($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Branch Hardware Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}