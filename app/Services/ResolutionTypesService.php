<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;


use App\Repositories\ResolutionTypesRepository;
use App\Services\Interfaces\ResolutionTypesInterface;


class ResolutionTypesService implements ResolutionTypesInterface
{
    /**
     * @var CustomersRepository
     */
    protected $repository;

    public function __construct(ResolutionTypesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAllResolutionTypes()
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $resoltiontypes = $this->repository->all();

            $dto->data = $resoltiontypes;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function pluckAllResolutionTypes($column, $key)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
            $resoltiontypes = $this->repository->pluck($column, $key);

            $dto->data = $resoltiontypes;

        } catch (\Exception $e) {

            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function getResolutionTypesByID($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $resoltiontypes = $this->repository->find($id);
            $dto->data = $resoltiontypes;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function createResolutionTypes($request)
    {

        $dto = new \App\Helpers\DTO;
        try {
            // dd($request->all());
            $resoltiontypes = $this->repository->create($request->all());

            $dto->message = 'Resolution Type created.';
            $dto->data = $resoltiontypes->toArray();

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;
    }

    public function updateResolutionTypes($request, $id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            //dd($id);
            $resoltiontypes = $this->repository->update($request->all(), $id);
            $dto->message = 'Resolution Type Updated.';
            $dto->data = $resoltiontypes;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }
        return $dto;

    }

    public function deleteResolutionTypes($id)
    {
        $dto = new \App\Helpers\DTO;
        try {
            $deleted = $this->repository->delete($id);

            $dto->message = 'Resolution Type Deleted.';
            $dto->data = $deleted;

        } catch (\Exception $e) {
            $dto->error = true;
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}