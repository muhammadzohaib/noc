<?php
/**
 * Created by PhpStorm.
 * User: Waqas
 * Date: 9/25/2017
 * Time: 12:53 PM
 */

namespace App\Services;

use App\Repositories\BranchesRepository;
use App\Repositories\ComplaintRepository;
use App\Repositories\CustomersRepository;
use App\Repositories\RegionsRepository;
use App\Repositories\ServicetypesRepository;
use App\Services\Interfaces\DashboardInterface;


class DashboardService implements DashboardInterface
{
    /**
     * @var ComplaintRepository
     */
    protected $repository;
    /**
     * @var RegionRepository
     */
    protected $regionrepository;
    /**
     * @var CustomerRepository
     */
    protected $customerrepository;
    /**
     * @var ServiceTypeRepository
     */
    protected $servicetyperepository;
    /**
     * @var BranchRepository
     */
    protected $branchrepository;
    public function __construct(ComplaintRepository $repository, RegionsRepository $regionrepository,CustomersRepository $customerrepository,ServicetypesRepository $servicetyperepository,BranchesRepository $branchrepository)

    {
        $this->repository = $repository;
        $this->regionrepository = $regionrepository;
        $this->customerrepository = $customerrepository;
        $this->servicetyperepository = $servicetyperepository;
        $this->branchrepository = $branchrepository;
    }

    public function getTotalComplaints($dtRange)
    {

        $dto = new \App\Helpers\DTO;
        try {
            if ($dtRange == 'Week') {
                $total = $this->repository->all()->where('created_at', '>=', now()->subWeek())->count();
            }
            else if ($dtRange == 'Month') {
                $total = $this->repository->all()->where('created_at', '>=', now()->subMonth())->count();
            }
            else {
                $total = $this->repository->all()->where('created_at', '>=', now()->subDay())->count();
            }

            $dto->data = $total;

        } catch (\Exception $e) {
            $dto->error = true;
            dd($e->getMessage());
            $dto->message = $e->getMessage();
        }

        return $dto;

    }

    public function getTotalResolvedComplaints($dtRange)
    {
        $dto = new \App\Helpers\DTO;
        try {
            if ($dtRange == 'Week') {
                $total = $this->repository->findByField('status', 'Resolved')->where('created_at', '>=', now()->subWeek())->where('resolved_at', '>', now()->subWeek())->count();
            }
            else if ($dtRange == 'Month') {
                $total = $this->repository->findByField('status', 'Resolved')->where('created_at', '>=', now()->subMonth())->where('resolved_at', '>', now()->subMonth())->count();
            }
            else  {
                $total = $this->repository->findByField('status', 'Resolved')->where('created_at', '>=', now()->subDay())->where('resolved_at', '>', now()->subDay())->count();
            }
            $dto->data = $total;

        } catch (\Exception $e) {
            $dto->error = true;
            dd($e->getMessage());
            $dto->message = $e->getMessage();
        }

        return $dto;
    }

    public function getTotalNotResolvedComplaints($dtRange)
    {

        $dto = new \App\Helpers\DTO;
        try {
            if ($dtRange == 'Week') {

                $total = $this->repository->findByField('status', 'Not Resolved')->where('created_at', '>=', now()->subWeek())->count();
          }
            else if ($dtRange == 'Month') {
                $total = $this->repository->findByField('status', 'Not Resolved')->where('created_at', '>=', now()->subMonth())->count();
            }
            else {
                $total = $this->repository->findByField('status', 'Not Resolved')->where('created_at', '>=', now()->subDay())->count();
            }
            $dto->data = $total;

        } catch (\Exception $e) {
            $dto->error = true;
            dd($e->getMessage());
            $dto->message = $e->getMessage();
        }

        return $dto;

    }

//    public function getRegionComplaints()
//    {
//
//        $dto = new \App\Helpers\DTO;
//        try {
//            $total = $this->regionrepository->whereHas(
//                'complaints',function ($q) {
//                $q->where('status', 'Not Resolved');
//            }
//            )->withCount([
//                'complaints',
//                'complaints AS complaints_count' => function ($q) {
//                    $q->where('status', 'Not Resolved');
//                }
//            ])->all();
//            // dd($total);
//            $dto->data = $total;
//
//        } catch (\Exception $e) {
//            $dto->error = true;
//            dd($e->getMessage());
//            $dto->message = $e->getMessage();
//        }
//
//        return $dto;
//
//    }

    public function getCustomerComplaints()
    {

        $dto = new \App\Helpers\DTO;
        try {

            $total = $this->customerrepository->withCount([
                'complaints',
                'complaints AS complaints_count' => function ($q) {
                    $q->where('status', 'Not Resolved');
                }
            ])->all();
          //  dd($total);
            $dto->data = $total;

        } catch (\Exception $e) {
            $dto->error = true;
            dd($e->getMessage());
            $dto->message = $e->getMessage();
        }

        return $dto;

    }

    public function getCustomerAtmComplaints()
    {

        $dto = new \App\Helpers\DTO;
        try {

            $total = $this->customerrepository->withCount([
                'complaints',
                'complaints AS complaints_count' => function ($q) {
                    $q->where('status', 'Not Resolved')->join("branches","branches.id","=","complaints.branch_id")->where('atm', '1');
                }
            ])->all();
          //  dd($total);
            $dto->data = $total;

        } catch (\Exception $e) {
            $dto->error = true;
            dd($e->getMessage());
            $dto->message = $e->getMessage();
        }

        return $dto;

    }


    public function getComplaintsByServiceTypes()
    {

        $dto = new \App\Helpers\DTO;
        try {

            $total = $this->servicetyperepository->whereHas(
                'complaints',function ($q) {
                    $q->where('status', 'Not Resolved');
                }
            )->withCount([
                'complaints',
                'complaints AS complaints_count' => function ($q) {
                    $q->where('status', 'Not Resolved');
                }
            ])->all();
           // dd($total);
            $dto->data = $total;

        } catch (\Exception $e) {
            $dto->error = true;
            dd($e->getMessage());
            $dto->message = $e->getMessage();
        }

        return $dto;

    }

    public function getOffCityComplaints()
    {

        $dto = new \App\Helpers\DTO;
        try {
            $total = $this->branchrepository->scopeQuery(function($query){
                return $query->groupBy('offcity');
            })
//                ->withCount([
//                'complaints',
//                'complaints AS complaints_count' => function ($q) {
//                    $q->where('status', 'Not Resolved');
//                }
//            ])
                ->all(['offcity']);
           // $total = $this->branchrepository;
          // dd($total);
            $dto->data = $total;

        } catch (\Exception $e) {
            $dto->error = true;
            dd($e->getMessage());
            $dto->message = $e->getMessage();
        }

        return $dto;

    }
}