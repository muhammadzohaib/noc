<?php

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class ComplaintValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'customer_id' => 'required',
            'servicetype_id' => 'required',
            'branch_id' => 'required',
            'supportstaff_id' => 'required',
            'status' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'supportstaff_id' => 'required',
            'status' => 'required',
        ],
    ];
}
