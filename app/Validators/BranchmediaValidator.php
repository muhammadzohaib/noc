<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class BranchmediaValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
		'required'	=>'	media_name=>required',
	],
        ValidatorInterface::RULE_UPDATE => [
		'required'	=>'	media_name=>required',
	],
   ];
}
