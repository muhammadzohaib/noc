<?php

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class BranchesValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'branch_code' => 'required',
            'branch_name' => 'required',
            'region_id' => 'required',
            'customer_id' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'branch_code' => 'required',
            'branch_name' => 'required',
            'region_id' => 'required',
            'customer_id' => 'required'
        ],
    ];
}
