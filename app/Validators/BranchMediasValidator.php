<?php

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class BranchMediasValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'media_name' => 'required',
            'branch_id' => 'required'
	],
        ValidatorInterface::RULE_UPDATE => [
            'media_name' => 'required',
            'branch_id' => 'required'
	],
   ];
}
