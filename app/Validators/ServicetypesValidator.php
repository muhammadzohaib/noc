<?php

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;


class ServicetypesValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title' => 'required',
        ],
    ];
}