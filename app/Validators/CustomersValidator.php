<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class CustomersValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
		    'alias'	=>'required',
            'name'	=>'required',
	],
        ValidatorInterface::RULE_UPDATE => [
		    'alias'	=>'required',
            'name'	=>'required',
	],
   ];
}
