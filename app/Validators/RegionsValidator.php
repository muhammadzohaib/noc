<?php

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class RegionsValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'region_code'	=>'required',
            'region_name'	=>'required',
	],
        ValidatorInterface::RULE_UPDATE => [
            'region_code'	=>'required',
            'region_name'	=>'required',
	],
   ];
}
