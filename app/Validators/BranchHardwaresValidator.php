<?php

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class BranchHardwaresValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'hardware_name' => 'required',
            'branch_id' => 'required'
	],
        ValidatorInterface::RULE_UPDATE => [
            'hardware_name' => 'required',
            'branch_id' => 'required'
	],
   ];
}
