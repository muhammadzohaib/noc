<?php

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UsersValidator extends LaravelValidator
{

    protected $rules = array(
        ValidatorInterface::RULE_CREATE => array(
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
        ),
        ValidatorInterface::RULE_UPDATE => array(
            'email' => 'sometimes|required|email',
            'password' => 'sometimes|required|string|min:6|confirmed',
        ),
    );
}
