<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Branches extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'branch_code',
        'branch_name',
        'address',
        'region_id',
        'offcity',
        'ip',
        'atm',
        'deleted_at',
        'loop_back_ip',
        'it_center',
        'khi_tunnel',
        'lhr_tunnel',
        'dxx_info',
        'bvpn_1_no',
        'bvpn_1_username',
        'bvpn_1_pw',
        'bvpn_2_no',
        'bvpn_2_username',
        'bvpn_2_pw',
        'evo_esn',
        'evo_mdn',
        'ospf_area',
        'branch_power_status',
        'complaint_description',
        'customer_id',
    ];

    public function region()
    {
        return $this->belongsTo('App\Entities\Regions', 'region_id');
    }

    public function branchcontacts()
    {
        return $this->hasMany('App\Entities\Branchcontacts');
    }

    public function complaints()
    {
        return $this->hasMany('App\Entities\Complaint','branch_id');
    }

    public function customers()
    {
        return $this->belongsTo('App\Entities\Customers', 'customer_id');
    }

}
