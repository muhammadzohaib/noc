<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Servicetypes extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'description',
        'deleted_at',
    ];

    public function complaints()
    {
        return $this->hasMany('App\Entities\Complaint','servicetype_id');
    }
}
