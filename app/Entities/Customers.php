<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Customers extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
		'alias' ,
		'name',
        'deleted_at',
	];

    public function complaints()
    {
        return $this->hasMany('App\Entities\Complaint','customer_id');
    }

    public function branches()
    {
        return $this->hasMany('App\Entities\Branches', 'branch_id');
    }
}
