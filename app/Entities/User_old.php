<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;


class UserOld extends Model implements Transformable
{

//    use Notifiable;
    use TransformableTrait;
    use SoftDeletes;

//    protected $fillable = [
//        'name', 'email', 'password','deleted_at',
//    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    public function setPasswordAttribute($password)
//    {
//        $this->attributes['password'] = bcrypt($password);
//    }
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
//    public function sendPasswordResetNotification($token)
//    {
//        $this->notify(new ResetPasswordNotification($token));
//    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

}
