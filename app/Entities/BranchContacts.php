<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class BranchContacts extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'contact',
        'extension',
        'comment',
        'branch_id',
        'deleted_at',
    ];

    public function branch()
    {
        return $this->belongsTo('App\Entities\Branches', 'branch_id');
    }

}
