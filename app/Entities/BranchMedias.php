<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class BranchMedias extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'branch_id',
        'media_name',
        'ip',
	];

    public function branch()
    {
        return $this->belongsTo('App\Entities\Branches', 'branch_id');
    }
}
