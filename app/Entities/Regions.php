<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Regions extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
		'region_code',
		'region_name',
        'manager_name',
        'manager_no',
        'deleted_at',
    ];

    public function supportstaff()
    {
        return $this->hasMany('App\Entities\Supportstaff');
    }

    public function branches()
    {
        return $this->hasMany('App\Entities\Branches', 'branch_id');
    }

}
