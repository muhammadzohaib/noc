<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Supportstaff extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'city',
        'address',
        'region_id',
        'deleted_at',
    ];

    public function region()
    {
        return $this->belongsTo('App\Entities\Regions', 'region_id');
    }

}
