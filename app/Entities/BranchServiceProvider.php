<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class BranchServiceProvider extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'branch_id',
        'name',
        'description',
        'code',
	];
    public function branch()
    {
        return $this->belongsTo('App\Entities\Branches', 'branch_id');
    }
}
