<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

//use Sofa\Eloquence\Eloquence;

class Complaint extends Model implements Transformable
{
    //   protected $searchableColumns = ['branch_id'];
    use TransformableTrait;

    protected $fillable = [
        'customer_id',
        'servicetype_id',
        'branch_id',
        'supportstaff_id',
        'status',
        'description',
        'complaint_description',
        'user_id',
        'resolutiontypes_id',
        'ptcl_ticket_no',
        'resolved_by',
        'resolved_at'
     ];

    public function customer()
    {
        return $this->belongsTo('App\Entities\Customers', 'customer_id');
    }

    public function servicetype()
    {
        return $this->belongsTo('App\Entities\Servicetypes', 'servicetype_id');
    }

    public function region()
    {
        return $this->belongsTo('App\Entities\Regions', 'region_id');
    }

    public function branch()
    {
        return $this->belongsTo('App\Entities\Branches', 'branch_id');
    }
    public function supportstaff()
    {
        return $this->belongsTo('App\Entities\Supportstaff', 'supportstaff_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id');
    }
    public function reolutiontypes()
    {
        return $this->belongsTo('App\Entities\Resolutiontypes', 'resolutiontypes_id');
    }
}
