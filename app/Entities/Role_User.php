<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Role_User extends Model
{
    protected $fillable = [
        'user_id',
        'role_id',
    ];
}
