<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\SupportstaffInterface as SupportstaffInterface;
use App\Validators\SupportstaffValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class SupportStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $supportstaffService;

    public function __construct(SupportstaffValidator $validator, SupportstaffInterface $supportstaffInterface)
    {
        $this->validator = $validator;
        $this->supportstaffService = $supportstaffInterface;
    }


    public function index()
    {
        $dto = $this->supportstaffService->getAllSupportstaff();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => $dto->message,
                'data' => $dto->data,
            ]);
        }
        //dd($dto->data);
        return view('supportstaffs.index', array('supportstaff' => $dto->data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('supportstaffs.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->supportstaffService->createSupportstaff($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->supportstaffService->getSupportstaffByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('supportstaffs.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supportstaff = $this->supportstaffService->getSupportstaffByID($id);
        if ($supportstaff->error) {
            return view('error', array('error' => $supportstaff->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('supportstaffs.edit', array('supportstaff' => $supportstaff->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $supportstaff = $this->supportstaffService->updateSupportstaff($request, $id);

            if ($supportstaff->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $supportstaff->message]);

                return view('error', array('error' => $supportstaff->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $supportstaff->message,
                    'data' => $supportstaff->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $supportstaff->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->supportstaffService->deleteSupportstaff($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Support Staff deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Support Staff deleted.');
    }
}
