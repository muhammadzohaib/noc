<?php

namespace App\Http\Controllers;


use App\Services\Interfaces\ComplaintInterface as ComplaintInterface;
use App\Services\Interfaces\DatatableInterface as DatatableInterface;
use App\Validators\ComplaintValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $complaintService;
    protected $datatableService;

    public function __construct(ComplaintValidator $validator, ComplaintInterface $complaintInterface, DatatableInterface $datatableInterface)
    {
        $this->validator = $validator;
        $this->ComplaintService = $complaintInterface;
        $this->DatatableService = $datatableInterface;
    }


    public function index()
    {
        return view('complaints.index');
    }

    public function unresolvedticket()
    {
        return view('complaints.unresolved');
    }

    public function resolvedindex()
    {
        return view('complaints.resolved');
    }

    public function getcomplaints()
    {
        $dto = $this->DatatableService->getComplaintsDatatable();

        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;
        }
    }

    public function getunresolvedtickets()
    {
        $dto = $this->DatatableService->getUnResolvedComplaintsDatatable();

        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }

    }

    public function getresolvedtickets()
    {
        $dto = $this->DatatableService->getResolvedComplaintsDatatable();

        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('complaints.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->ComplaintService->createComplaint($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->ComplaintService->getComplaintsByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('complaints.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $complaints = $this->ComplaintService->getComplaintsByID($id);
        if ($complaints->error) {
            return view('error', array('error' => $complaints->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('complaints.edit', array('complaints' => $complaints->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $complaints = $this->ComplaintService->updateComplaint($request, $id);

            if ($complaints->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $complaints->message]);

                return view('error', array('error' => $complaints->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $complaints->message,
                    'data' => $complaints->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $complaints->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->ComplaintService->deleteComplaint($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Complaint deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Complaint deleted.');
    }
}
