<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\RegionInterface as RegionInterface;
use App\Validators\RegionsValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class RegionsController extends Controller
{
    /**
     * @var RegionsValidator
     */
    protected $validator;
    protected $regionService;

    public function __construct(RegionsValidator $validator, RegionInterface $regionInterface)
    {
        $this->validator  = $validator;
        $this->regionService = $regionInterface;
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dto = $this->regionService->getAllRegions();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => $dto->message,
                'data' => $dto->data,
            ]);
        }

        return view('regions.index', array('regions' => $dto->data));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('regions.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->regionService->createRegion($request);

            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }


    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->regionService->getRegionByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('regions.show', compact($dto->data));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $region = $this->regionService->getRegionByID($id);
        if ($region->error) {
            return view('errors.503', array('error' => $region->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('regions.edit', array('region' => $region->data))->with(compact('validator'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $region = $this->regionService->updateRegion($request, $id);

            if ($region->error) {
                if ($request->wantsJson())
                    return response()->json($region->message);

                return view('errors.503', array('error' => $region->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $region->message,
                    'data' => $region->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $region->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->regionService->deleteRegion($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('errors.503', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Region deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Region deleted.');
    }
}