<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\CustomerInterface as CustomerInterface;
use App\Validators\CustomersValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class CustomersController extends Controller
{
    /**
     * @var CustomersValidator
     */
    protected $validator;
    protected $customerService;

    public function __construct(CustomersValidator $validator, CustomerInterface $customerInterface)
    {
        $this->validator  = $validator;
        $this->customerService = $customerInterface;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dto = $this->customerService->getAllCustomers();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => $dto->message,
                'data' => $dto->data,
            ]);
        }

        return view('customers.index', array('customers' => $dto->data));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('customers.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->customerService->createCustomer($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->customerService->getCustomerByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('customers.show', compact($dto->data));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = $this->customerService->getCustomerByID($id);
        if ($customer->error) {
            return view('errors.503', array('error' => $customer->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('customers.edit', array('customer' => $customer->data))->with(compact('validator'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
           $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $customer = $this->customerService->updateCustomer($request, $id);

            if ($customer->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $customer->message]);

                return view('error', array('error' => $customer->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $customer->message,
                    'data' => $customer->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $customer->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->customerService->deleteCustomer($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('errors.503', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Customer deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Customer deleted.');
    }
}