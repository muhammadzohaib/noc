<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\ServiceTypesInterface as ServiceTypesInterface;
use App\Validators\ServicetypesValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $servicetypeService;

    public function __construct(ServicetypesValidator $validator, ServiceTypesInterface $servicetypeInterface)
    {
        $this->validator = $validator;
        $this->ServiceTypeService = $servicetypeInterface;
    }


    public function index()
    {
        $dto = $this->ServiceTypeService->getAllServiceTypes();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => $dto->message,
                'data' => $dto->data,
            ]);
        }

        return view('servicetypes.index', array('servicetypes' => $dto->data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('servicetypes.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->ServiceTypeService->createServiceType($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->ServiceTypeService->getSevriceTypesByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('servicetypes.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicetypes = $this->ServiceTypeService->getSevriceTypesByID($id);
        if ($servicetypes->error) {
            return view('error', array('error' => $servicetypes->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('servicetypes.edit', array('servicetypes' => $servicetypes->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $servicetypes = $this->ServiceTypeService->updateServiceType($request, $id);

            if ($servicetypes->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $servicetypes->message]);

                return view('error', array('error' => $servicetypes->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $servicetypes->message,
                    'data' => $servicetypes->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $servicetypes->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->ServiceTypeService->deleteServiceType($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Service Type deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Service Type deleted.');
    }
}
