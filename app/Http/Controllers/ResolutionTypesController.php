<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\ResolutionTypesInterface as ResolutionTypesInterface;
use App\Validators\ResolutionTypesValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ResolutionTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $resolutiontypesService;

    public function __construct(ResolutionTypesValidator $validator, ResolutionTypesInterface $resolutiontypesInterface)
    {
        $this->validator  = $validator;
        $this->ResolutionTypesService = $resolutiontypesInterface;
    }
    public function index()
    {
        $dto = $this->ResolutionTypesService->getAllResolutionTypes();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => $dto->message,
                'data' => $dto->data,
            ]);
        }

        return view('resolutiontypes.index', array('resolutiontypes' => $dto->data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resolutiontypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->ResolutionTypesService->createResolutionTypes($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->ResolutionTypesService->getResolutionTypesByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('resolutiontypes.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resolutiontypes = $this->ResolutionTypesService->getResolutionTypesByID($id);
        if ($resolutiontypes->error) {
            return view('error', array('error' => $resolutiontypes->message));
        }
        return view('resolutiontypes.edit', array('resolutiontypes' => $resolutiontypes->data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $resolutiontypes = $this->ResolutionTypesService->updateResolutionTypes($request, $id);

            if ($resolutiontypes->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $resolutiontypes->message]);

                return view('error', array('error' => $resolutiontypes->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $resolutiontypes->message,
                    'data' => $resolutiontypes->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $resolutiontypes->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->ResolutionTypesService->deleteResolutionTypes($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Resolution Type deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Resolution Type deleted.');
    }
}
