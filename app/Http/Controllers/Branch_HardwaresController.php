<?php

namespace App\Http\Controllers;


use App\Services\Interfaces\BranchHardwaresInterface as BranchHardwaresInterface;
use App\Services\Interfaces\DatatableInterface;
use App\Validators\BranchHardwaresValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class Branch_HardwaresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $branchhardwaresService;
    protected $datatableService;

    public function __construct(BranchHardwaresValidator $validator, BranchHardwaresInterface $branchhardwaresService, DatatableInterface $datatableInterface)
    {
        $this->validator = $validator;
        $this->BranchHardwaresService = $branchhardwaresService;
        $this->DatatableService = $datatableInterface;
    }


    public function index()
    {

        return view('branchhardwares.index');
    }

    public function getbranchhardwares()
    {
        $dto = $this->DatatableService->getBranchHardwaresDatatable();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('branchhardwares.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->BranchHardwaresService->createBranch_Hardwares($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->BranchHardwaresService->getBranch_HardwaresByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('branchhardwares.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branchhardwares = $this->BranchHardwaresService->getBranch_HardwaresByID($id);
        if ($branchhardwares->error) {
            return view('error', array('error' => $branchhardwares->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('branchhardwares.edit', array('branchhardwares' => $branchhardwares->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $branchhardwares = $this->BranchHardwaresService->updateBranch_Hardwares($request, $id);

            if ($branchhardwares->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $branchhardwares->message]);

                return view('error', array('error' => $branchhardwares->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $branchhardwares->message,
                    'data' => $branchhardwares->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $branchhardwares->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->BranchHardwaresService->deleteBranch_Hardwares($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Branch Hardware deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Branch Hardware deleted.');
    }
}
