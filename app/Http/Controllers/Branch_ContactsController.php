<?php

namespace App\Http\Controllers;


use App\Services\Interfaces\BranchContactsInterface as BranchContactsInterface;
use App\Services\Interfaces\DatatableInterface;
use App\Validators\BranchContactsValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class Branch_ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $branchcontactsService;
    protected $datatableService;

    public function __construct(BranchContactsValidator $validator, BranchContactsInterface $branchcontactsInterface, DatatableInterface $datatableInterface)
    {
        $this->validator = $validator;
        $this->BranchContactsService = $branchcontactsInterface;
        $this->DatatableService = $datatableInterface;
    }


    public function index()
    {
       return view('branchcontacts.index');
    }

    public function getbranchcontacts()
    {
        $dto = $this->DatatableService->getBranchContactsDatatable();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('branchcontacts.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->BranchContactsService->createBranch_Contacts($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->BranchContactsService->getBranch_ContactsByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('branchcontacts.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branchcontacts = $this->BranchContactsService->getBranch_ContactsByID($id);
        if ($branchcontacts->error) {
            return view('error', array('error' => $branchcontacts->message));
        }

        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('branchcontacts.edit', array('branchcontacts' => $branchcontacts->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $branchcontacts = $this->BranchContactsService->updateBranch_Contacts($request, $id);

            if ($branchcontacts->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $branchcontacts->message]);

                return view('error', array('error' => $branchcontacts->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $branchcontacts->message,
                    'data' => $branchcontacts->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $branchcontacts->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->BranchContactsService->deleteBranch_Contact($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Branch Contact deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Branch Contact deleted.');
    }
}
