<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\PostRepository;
use App\Validators\PostValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;


class AdminController extends Controller
{

    public function index()
    {
        // if user is login and is admins then
        return view('admins.index');
        // else : show him login page

    }

}
