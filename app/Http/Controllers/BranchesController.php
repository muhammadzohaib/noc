<?php

namespace App\Http\Controllers;


use App\Services\Interfaces\BranchesInterface as BranchesInterface;
use App\Services\Interfaces\DatatableInterface as DatatableInterface;
use App\Validators\BranchesValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $branchesService;
    protected $datatableService;

    public function __construct(BranchesValidator $validator, BranchesInterface $branchesInterface, DatatableInterface $datatableInterface)
    {

        $this->validator = $validator;
        $this->BranchesService = $branchesInterface;
        $this->DatatableService = $datatableInterface;
    }


    public function index()
    {

        return view('branches.index');
    }

    public function getbranches()
    {
        $dto = $this->DatatableService->getBranchesDatatable();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }

    }

    public function getBranchesByCustomer()
    {
//        $this->repository->pushCriteria(new BranchCriteria());
        $dto = $this->BranchesService->getBranchesbyCustomer();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('branches.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->BranchesService->createBranches($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->BranchesService->getBranchesByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('branches.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branches = $this->BranchesService->getBranchesByID($id);
        if ($branches->error) {
            return view('error', array('error' => $branches->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('branches.edit', array('branches' => $branches->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $branches = $this->BranchesService->updateBranches($request, $id);

            if ($branches->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $branches->message]);

                return view('error', array('error' => $branches->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $branches->message,
                    'data' => $branches->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $branches->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->BranchesService->deleteBranch($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Branch deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Branch deleted.');
    }
}
