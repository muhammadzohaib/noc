<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\DatatableInterface;
use App\Services\Interfaces\UserInterface as UserInterface;
use App\Validators\UsersValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $usersService;
    protected $datatableService;

    public function __construct(UsersValidator $validator, UserInterface $usersInterface, DatatableInterface $datatableInterface)
    {
        $this->validator = $validator;
        $this->UsersService = $usersInterface;
        $this->DatatableService = $datatableInterface;
    }


    public function index()
    {
        return view('users.index');
    }

    public function userslist()
    {

      return view('users.userslist');
    }

    public function getuserslist()
    {
        $dto = $this->DatatableService->getUsersDatatable();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('users.create')->with(compact('validator'));
//        return view('auth.passwords.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->UsersService->createUser($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->UsersService->getUserByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('users.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = $this->UsersService->getUserByID($id);
        if ($users->error) {
            return view('error', array('error' => $users->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('users.edit', array('users' => $users->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $users = $this->UsersService->updateUser($request, $id);

            if ($users->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $users->message]);

                return view('error', array('error' => $users->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $users->message,
                    'data' => $users->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $users->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->UsersService->deleteUser($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'User deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'User deleted.');
    }

    public function changePassword()
    {
        return view('users.changepassword');
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function updatepassword(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $users = $this->UsersService->updateUserPassword($request);

            if ($users->error) {
                return redirect()->back()->withErrors($users->message);
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $users->message,
                    'data' => $users->data->toArray()
                ]);
            }
            return redirect()->back()->with('message', $users->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }
}
