<?php

namespace App\Http\Controllers;

use App\Services\Interfaces\DashboardInterface as DashboardInterface;
use Illuminate\Http\Request;

class ManagersController extends Controller
{
    /**
     * @var DashboardInterface
     */
    protected $dashboardService;

    public function __construct(DashboardInterface $dashboardInterface)
    {
        $this->dashboardService = $dashboardInterface;
    }

    public function index(Request $request)
    {
//        dd($request->daterange);
        $totalResolvedComplaints = $this->dashboardService->getTotalResolvedComplaints($request->daterange)->data;
        $totalComplaints = $this->dashboardService->getTotalComplaints($request->daterange)->data;
        $totalNotResolvedComplaints = $this->dashboardService->getTotalNotResolvedComplaints($request->daterange)->data;
//        $getRegionComplaints = $this->dashboardService->getRegionComplaints()->data;
        $getCustomerComplaints = $this->dashboardService->getCustomerComplaints()->data;
        $getComplaintsByServiceTypes = $this->dashboardService->getComplaintsByServiceTypes()->data;
        $getOffCityComplaints = $this->dashboardService->getOffCityComplaints()->data;
        $getCustomerAtmComplaints = $this->dashboardService->getCustomerAtmComplaints()->data;
        return view('managers.index',
                compact(['totalResolvedComplaints','totalComplaints','totalNotResolvedComplaints','getRegionComplaints','getCustomerComplaints','getComplaintsByServiceTypes','getOffCityComplaints','getCustomerAtmComplaints']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
