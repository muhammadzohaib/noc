<?php

namespace App\Http\Controllers;


use App\Services\Interfaces\BranchServiceProvidersInterface as BranchServiceProvidersInterface;
use App\Services\Interfaces\DatatableInterface;
use App\Validators\BranchServiceProviderValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class Branch_ServiceProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $branchserviceprovidersService;
    protected $datatableService;

    public function __construct(BranchServiceProviderValidator $validator, BranchServiceProvidersInterface $branchserviceprovidersService, DatatableInterface $datatableInterface)
    {
        $this->validator = $validator;
        $this->BranchServiceProvidersService = $branchserviceprovidersService;
        $this->DatatableService = $datatableInterface;
    }


    public function index()
    {
        return view('branchserviceproviders.index');

    }

    public function getserviceproviders()
    {
        $dto = $this->DatatableService->getBranchServiceProvidersDatatable();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('branchserviceproviders.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->BranchServiceProvidersService->createBranch_ServiceProviders($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->BranchServiceProvidersService->getBranch_ServiceProvidersByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('branchserviceproviders.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branchserviceproviders = $this->BranchServiceProvidersService->getBranch_ServiceProvidersByID($id);
        if ($branchserviceproviders->error) {
            return view('error', array('error' => $branchserviceproviders->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('branchserviceproviders.edit', array('branchserviceproviders' => $branchserviceproviders->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $branchserviceproviders = $this->BranchServiceProvidersService->updateBranch_ServiceProviders($request, $id);

            if ($branchserviceproviders->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $branchserviceproviders->message]);

                return view('error', array('error' => $branchserviceproviders->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $branchserviceproviders->message,
                    'data' => $branchserviceproviders->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $branchserviceproviders->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->BranchServiceProvidersService->deleteBranch_ServiceProviders($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Branch Service Provider deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Branch Service Provider deleted.');
    }
}