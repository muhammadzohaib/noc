<?php

namespace App\Http\Controllers;


use App\Services\Interfaces\BranchMediasInterface as BranchMediasInterface;
use App\Services\Interfaces\DatatableInterface;
use App\Validators\BranchMediasValidator;
use Illuminate\Http\Request;
use JsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class Branch_MediasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $validator;
    protected $branchmediasService;
    protected $datatableService;

    public function __construct(BranchMediasValidator $validator, BranchMediasInterface $branchmediasService, DatatableInterface $datatableInterface)
    {
        $this->validator = $validator;
        $this->BranchMediasService = $branchmediasService;
        $this->DatatableService = $datatableInterface;
    }


    public function index()
    {
        return view('branchmedias.index');
    }

    public function getbranchmedias()
    {
        $dto = $this->DatatableService->getBranchMediasDatatable();
        if ($dto->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $dto->message,
                ]);
            }
            return view('error', array('error' => $dto->message));
        } else {

            return $dto->data;

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_CREATE));
        return view('branchmedias.create')->with(compact('validator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $dto = $this->BranchMediasService->createBranch_Medias($request);
            if ($dto->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $dto->message]);

                return view('error', array('error' => $dto->message));
            }
            if ($request->wantsJson()) {
                return response()->json($dto);
            }

            return redirect()->back()->with('message', $dto->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                    'data' => null
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dto = $this->BranchMediasService->getBranch_MediasByID($id);

        if (request()->wantsJson()) {

            return response()->json([
                'error' => $dto->error,
                'data' => $dto->data
            ]);
        }

        return view('branchmedias.show', compact($dto->data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branchmedias = $this->BranchMediasService->getBranch_MediasByID($id);
        if ($branchmedias->error) {
            return view('error', array('error' => $branchmedias->message));
        }
        $validator = JsValidator::make($this->validator->getRules(ValidatorInterface::RULE_UPDATE));
        return view('branchmedias.edit', array('branchmedias' => $branchmedias->data))->with(compact('validator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $branchmedias = $this->BranchMediasService->updateBranch_Medias($request, $id);

            if ($branchmedias->error) {
                if ($request->wantsJson())
                    return response()->json(['error' => true, 'message' => $branchmedias->message]);

                return view('error', array('error' => $branchmedias->message));
            }
            if ($request->wantsJson()) {
                return response()->json([
                    'message' => $branchmedias->message,
                    'data' => $branchmedias->data->toArray()
                ]);
            }

            return redirect()->back()->with('message', $branchmedias->message);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->BranchMediasService->deleteBranch_Medias($id);

        if ($deleted->error) {
            if (request()->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $deleted->message
                ]);
            }
            return view('error', array('error' => $deleted->message));
        }
        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'Branch Media deleted.'
            ]);
        }
        return redirect()->back()->with('message', 'Branch Media deleted.');
    }
}
