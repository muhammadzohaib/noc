<?php

namespace App\Http\ViewComposers;

use App\Services\Interfaces\BranchesInterface;
use Illuminate\View\View;

class BranchComposer
{
    /**
     * The role service implementation.
     *
     * @var BranchesInterface
     */
    protected $branchService;

    /**
     * Create a new branch composer.
     *
     * @param  BranchesInterface $branchInterface
     * @return void
     */
    public function __construct(BranchesInterface $branchInterface)
    {
        // Dependencies automatically resolved by service container...
        $this->branchService = $branchInterface;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $branches = $this->branchService->pluckAllBranches('branch_name', 'id');
        if ($branches->error) {
            \App::abort(403, 'Error in Branch Composer');

        }
        $branches = $branches->data;
        $view->with(compact('branches'));


    }
}