<?php

namespace App\Http\ViewComposers;

use App\Services\Interfaces\CustomerInterface;
use Illuminate\View\View;

class CustomerComposer
{
    /**
     * The role service implementation.
     *
     * @var CustomerInterface
     */
    protected $customerService;

    /**
     * Create a new customer composer.
     *
     * @param  CustomerInterface $customerInterface
     * @return void
     */
    public function __construct(CustomerInterface $customerInterface)
    {
        // Dependencies automatically resolved by service container...
        $this->customerService = $customerInterface;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $customers = $this->customerService->pluckAllCustomers('alias', 'id');
        if ($customers->error) {
            \App::abort(403, 'Error in Customer Composer');

        }
        $customers = $customers->data;
        $view->with(compact('customers'));


    }
}