<?php

namespace App\Http\ViewComposers;

use App\Services\Interfaces\RegionInterface;
use Illuminate\View\View;
use Redirect;

class RegionComposer
{
    /**
     * The role service implementation.
     *
     * @var RegionInterface
     */
    protected $regionService;

    /**
     * Create a new region composer.
     *
     * @param  RegionInterface $regionInterface
     * @return void
     */
    public function __construct(RegionInterface $regionInterface)
    {
        // Dependencies automatically resolved by service container...
        $this->regionService = $regionInterface;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $regions = $this->regionService->pluckAllRegions('region_name', 'id');
        if ($regions->error) {
            \App::abort(403, 'Error in Region Composer');

        }
        $regions = $regions->data;
        $view->with(compact('regions'));


    }
}