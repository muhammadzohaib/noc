<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 10/20/2017
 * Time: 12:29 PM
 */

namespace App\Http\ViewComposers;


use App\Services\Interfaces\ResolutionTypesInterface;
use Illuminate\View\View;

class ResolutionTypesComposer
{
    /**
     * The role resolution implementation.
     *
     * @var ResolutiontypesInterface
     */
    protected $resolutiontypesService;

    /**
     * Create a new resolutiontypes composer.
     *
     * @param  ServicetypesInterface $resolutiontypesInterface
     * @return void
     */
    
    public function __construct(ResolutionTypesInterface $resolutiontypesInterface)
    {
        // Dependencies automatically resolved by service container...
        $this->resolutiontypesService = $resolutiontypesInterface;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $resolutiontypes = $this->resolutiontypesService->pluckAllResolutiontypes('name', 'id');
        if ($resolutiontypes->error) {
            \App::abort(403, 'Error in Resolution Type Composer');

        }
        $resolutiontypes = $resolutiontypes->data;
        $view->with(compact('resolutiontypes'));


    }
}