<?php

namespace App\Http\ViewComposers;

use App\Services\Interfaces\ServiceTypesInterface;
use Illuminate\View\View;

class ServicetypeComposer
{
    /**
     * The role service implementation.
     *
     * @var ServicetypesInterface
     */
    protected $servicetypeService;

    /**
     * Create a new servicetype composer.
     *
     * @param  ServicetypesInterface $servicetypeInterface
     * @return void
     */
    public function __construct(ServiceTypesInterface $servicetypeInterface)
    {
        // Dependencies automatically resolved by service container...
        $this->servicetypeService = $servicetypeInterface;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $servicetypes = $this->servicetypeService->pluckAllServicetypes('title', 'id');
        if ($servicetypes->error) {
            \App::abort(403, 'Error in Service Type Composer');

        }
        $servicetypes = $servicetypes->data;
        $view->with(compact('servicetypes'));


    }
}