<?php

namespace App\Http\ViewComposers;

use App\Services\Interfaces\SupportstaffInterface;
use Illuminate\View\View;

class SupportstaffComposer
{
    /**
     * The role service implementation.
     *
     * @var SupportstaffInterface
     */
    protected $supportstaffService;

    /**
     * Create a new supportstaff composer.
     *
     * @param  SupportstaffInterface $supportstaffInterface
     * @return void
     */
    public function __construct(SupportstaffInterface $supportstaffInterface)
    {
        // Dependencies automatically resolved by service container...
        $this->supportstaffService = $supportstaffInterface;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {

        $supportstaffs = $this->supportstaffService->pluckAllSupportstaffs('name', 'id');
        //dd($supportstaffs ;
        if ($supportstaffs->error) {
            \App::abort(403, 'Error in Support Staff Composer');

        }
        $supportstaffs = $supportstaffs->data;
        $view->with(compact('supportstaffs'));


    }
}