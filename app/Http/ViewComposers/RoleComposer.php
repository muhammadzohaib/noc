<?php

namespace App\Http\ViewComposers;

use App\Services\Interfaces\RoleInterface;
use Illuminate\View\View;

class RoleComposer
{
    /**
     * The role service implementation.
     *
     * @var BranchesInterface
     */
    protected $roleService;

    /**
     * Create a new branch composer.
     *
     * @param  BranchesInterface $branchInterface
     * @return void
     */
    public function __construct(RoleInterface $roleInterface)
    {
        // Dependencies automatically resolved by service container...
        $this->roleService = $roleInterface;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $roles = $this->roleService->pluckAllRoles('name', 'id');
        if ($roles->error) {
            \App::abort(403, 'Error in Role Composer');

        }
        $roles = $roles->data;
        $view->with(compact('roles'));


    }
}