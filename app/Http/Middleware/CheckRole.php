<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->hasRole('manager')) {
                return redirect('managers');
            } else if (Auth::user()->hasRole('admin')) {
                return redirect('admins');
            } else if (Auth::user()->hasRole('user')) {
                return redirect('users');
            }
            else if (Auth::user()->hasRole('NBP')) {
                return redirect('nbp');
            }
        }
        return $next($request);
    }
}
