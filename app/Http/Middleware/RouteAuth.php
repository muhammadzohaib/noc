<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RouteAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->hasRole('user')) {
                return abort(404);
            }
            return $next($request);
        }
        return redirect('/');
    }

}
