<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\BranchServiceProvider;

/**
 * Class BranchServiceProviderTransformer
 * @package namespace App\Transformers;
 */
class BranchServiceProviderTransformer extends TransformerAbstract
{

    /**
     * Transform the \BranchServiceProvider entity
     * @param \BranchServiceProvider $model
     *
     * @return array
     */
    public function transform(BranchServiceProvider $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
