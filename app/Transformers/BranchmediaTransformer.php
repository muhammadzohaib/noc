<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Branchmedia;

/**
 * Class BranchmediaTransformer
 * @package namespace App\Transformers;
 */
class BranchmediaTransformer extends TransformerAbstract
{

    /**
     * Transform the \Branchmedia entity
     * @param \Branchmedia $model
     *
     * @return array
     */
    public function transform(Branchmedia $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
