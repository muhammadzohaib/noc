<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Customers;

/**
 * Class CustomersTransformer
 * @package namespace App\Transformers;
 */
class CustomersTransformer extends TransformerAbstract
{

    /**
     * Transform the \Customers entity
     * @param \Customers $model
     *
     * @return array
     */
    public function transform(Customers $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
