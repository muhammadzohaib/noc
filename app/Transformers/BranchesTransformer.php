<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class BranchesTransformer
 * @package namespace App\Transformers;
 */
class BranchesTransformer extends TransformerAbstract
{

    /**
     * Transform the \Branches entity
     * @param \Branches $model
     *
     * @return array
     */
    public function transform(Collection $model)
    {
        return [
            'id' => (int)$model->id,
            'branch_name' => $model->branch_name,
            'region_name' => $model->region->region_name,
            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
