<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\BranchMedias;

/**
 * Class BranchMediasTransformer
 * @package namespace App\Transformers;
 */
class BranchMediasTransformer extends TransformerAbstract
{

    /**
     * Transform the \BranchMedias entity
     * @param \BranchMedias $model
     *
     * @return array
     */
    public function transform(BranchMedias $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
