<?php

namespace App\Transformers;

use App\Entities\BranchContacts;
use League\Fractal\TransformerAbstract;

/**
 * Class BranchContactsTransformer
 * @package namespace App\Transformers;
 */
class BranchContactsTransformer extends TransformerAbstract
{

    /**
     * Transform the \BranchContacts entity
     * @param \BranchContacts $model
     *
     * @return array
     */
    public function transform(BranchContacts $model)
    {
        return [
            'id' => (int)$model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
