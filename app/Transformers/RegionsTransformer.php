<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Regions;

/**
 * Class RegionsTransformer
 * @package namespace App\Transformers;
 */
class RegionsTransformer extends TransformerAbstract
{

    /**
     * Transform the \Regions entity
     * @param \Regions $model
     *
     * @return array
     */
    public function transform(Regions $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
