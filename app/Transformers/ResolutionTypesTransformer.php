<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\ResolutionTypes;

/**
 * Class ResolutionTypesTransformer
 * @package namespace App\Transformers;
 */
class ResolutionTypesTransformer extends TransformerAbstract
{

    /**
     * Transform the \ResolutionTypes entity
     * @param \ResolutionTypes $model
     *
     * @return array
     */
    public function transform(ResolutionTypes $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
