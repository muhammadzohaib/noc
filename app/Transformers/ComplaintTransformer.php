<?php

namespace App\Transformers;

use App\Entities\Complaint;
use League\Fractal\TransformerAbstract;

/**
 * Class ComplaintTransformer
 * @package namespace App\Transformers;
 */
class ComplaintTransformer extends TransformerAbstract
{

    /**
     * Transform the \Complaint entity
     * @param \Complaint $model
     *
     * @return array
     */
    public function transform(Complaint $model)
    {
        return [
            'id' => (int)$model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
