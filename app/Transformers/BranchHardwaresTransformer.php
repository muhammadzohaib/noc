<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\BranchHardwares;

/**
 * Class BranchHardwaresTransformer
 * @package namespace App\Transformers;
 */
class BranchHardwaresTransformer extends TransformerAbstract
{

    /**
     * Transform the \BranchHardwares entity
     * @param \BranchHardwares $model
     *
     * @return array
     */
    public function transform(BranchHardwares $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
