<?php

namespace App\Transformers;

use App\Entities\Servicetypes;
use League\Fractal\TransformerAbstract;

/**
 * Class ServicetypesTransformer
 * @package namespace App\Transformers;
 */
class ServicetypesTransformer extends TransformerAbstract
{

    /**
     * Transform the \Servicetypes entity
     * @param \Servicetypes $model
     *
     * @return array
     */
    public function transform(Servicetypes $model)
    {
        return [
            'id' => (int)$model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
