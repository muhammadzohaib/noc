<?php

namespace App\Transformers;

use App\Entities\Supportstaff;
use League\Fractal\TransformerAbstract;

/**
 * Class SupportstaffTransformer
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{

    /**
     * Transform the \Supportstaff entity
     * @param \Supportstaff $model
     *
     * @return array
     */
    public function transform(Supportstaff $model)
    {
        return [
            'id' => (int)$model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
