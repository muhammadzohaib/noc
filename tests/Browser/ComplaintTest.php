<?php

namespace Tests\Browser;

use App\Entities\Complaint;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ComplaintTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testInsertComplaint()
    {

        $this->browse(function ($first) {

            $first->loginAs(\App\User::find(2))
               ->visit('/complaints/create')
                ->select('customer_id', '1')
                ->select('servicetype_id', '1')
                ->type('complaint_description', 'test complaint description')
                ->select('branch_id', '1')
                ->select('supportstaff_id', '1')
                ->select('status', 'Not Resolved')
                ->type('description', 'test description')
                 ->press('Add Complaint')
                ->assertSee('Complaint created.');

        });
    }


    public function testUpdateComplaint()
    {

        $complaint = factory(Complaint::class)->create();

        $this->browse(function ($first)  use ($complaint) {

            $first->loginAs(\App\User::find(2))
                ->visit(new Pages\ComplaintsEditPage($complaint))
                ->select('customer_id', '2')
                ->select('servicetype_id', '1')
                ->type('complaint_description', 'test update complaint description')
                ->select('branch_id', '1')
                ->select('supportstaff_id', '1')
                ->select('status', 'Not Resolved')
                ->type('description', 'test description')
                ->press('Update Complaint')
                ->assertSee('Complaint Updated.');
        });
    }
}
