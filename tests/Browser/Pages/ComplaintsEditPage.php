<?php
/**
 * Created by PhpStorm.
 * User: f
 * Date: 10/16/2017
 * Time: 6:19 PM
 */

namespace Tests\Browser\Pages;


use App\Entities\Complaint;


class ComplaintsEditPage extends Page
{
    protected $complaint;

    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
      //  dd($complaint);
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/complaints/' . $this->complaint->id . '/edit';
    }
}