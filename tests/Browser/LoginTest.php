<?php

namespace Tests\Browser;

use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $manager = \App\User::where('email', 'manager@noc.com')->first();
        $admin = \App\User::where('email', 'admin@noc.com')->first();
        $user = \App\User::where('email', 'user@noc.com')->first();

        $this->browse(function ($first, $second, $third) use ($manager, $admin, $user) {

            $first->visit('/')
                ->type('email', $manager->email)
                ->type('password', '123456')
                ->press('Login')
                ->assertPathIs('/managers');
//            $second->visit('/')
//                ->type('email', $admin->email)
//                ->type('password', '123456')
//                ->press('Login')
//                ->assertPathIs('/admins');
//            $third->visit('/')
//                ->type('email', $user->email)
//                ->type('password', '123456')
//                ->press('Login')
//                ->assertPathIs('/users');
        });
    }
}
