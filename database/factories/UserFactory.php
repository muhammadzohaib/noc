<?php

use Faker\Generator as Faker;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10)
    ];


});

$factory->define(App\Role::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];


});


//$factory->define(App\Entities\Role_User::class, function (Faker $faker) {
//    $roles = App\Role::pluck('id')->all();
//    return [
//        'user_id'     => factory('App\Entities\User')->create()->id,
//        'role_id' => $faker->randomElement($roles)
//    ];
//});

$factory->define(App\Entities\Servicetypes::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->text
    ];
});

$factory->define(App\Entities\Customers::class, function (Faker $faker) {
    return [
        'alias' => $faker->name,
        'name' => $faker->name
    ];
});
$factory->define(App\Entities\Regions::class, function (Faker $faker) {
    return [
        'region_code' => $faker->randomNumber(),
        'region_name' => $faker->randomElement(['NORTH', 'CENTRAL', 'SOUTH']),
        'manager_name' => $faker->name,
        'manager_no' => $faker->randomNumber()
    ];
});

$factory->define(App\Entities\Branches::class, function (Faker $faker) {
    $regions = App\Entities\Regions::pluck('id')->all();
    $customers = App\Entities\Customers::pluck('id')->all();
    return [
        'branch_code' => $faker->randomNumber(),
        'branch_name' => $faker->name,
        'address' => $faker->address,
        'region_id' => $faker->randomElement($regions),
        'offcity' => $faker->randomElement(['1', '0']),
        'ip' => $faker->ipv4,
        'atm' => $faker->randomElement(['1', '0']),
        'loop_back_ip' => $faker->ipv4,
        'it_center' => $faker->text,
        'khi_tunnel' => $faker->text,
        'lhr_tunnel' => $faker->text,
        'dxx_info' => $faker->text,
        'bvpn_1_no' => $faker->randomNumber(),
        'bvpn_1_username' => $faker->name,
        'bvpn_1_pw' => $faker->password,
        'bvpn_2_no' => $faker->randomNumber(),
        'bvpn_2_username' => $faker->name,
        'bvpn_2_pw' => $faker->password,
        'evo_esn' => $faker->text,
        'evo_mdn' => $faker->text,
        'ospf_area' => $faker->text,
        'branch_power_status' => $faker->randomElement(['1', '0']),
        'customer_id' => $faker->randomElement($customers),
    ];
});

$factory->define(App\Entities\BranchContacts::class, function (Faker $faker) {
    $branches = App\Entities\Branches::pluck('id')->all();
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'contact' => $faker->randomNumber(),
        'extension' => $faker->randomNumber(),
        'comment' => $faker->text,
        'branch_id' => $faker->randomElement($branches)
    ];
});


$factory->define(App\Entities\Supportstaff::class, function (Faker $faker) {
    $regions = App\Entities\Regions::pluck('id')->all();
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->randomNumber(),
        'city' => $faker->city,
        'address' => $faker->address,
        'region_id' => $faker->randomElement($regions)
    ];
});

$factory->define(App\Entities\Complaint::class, function (Faker $faker) {
    $customers = App\Entities\Customers::pluck('id')->all();
    $users = App\User::pluck('id')->all();
    $services = App\Entities\Servicetypes::pluck('id')->all();
    $branches = App\Entities\Branches::pluck('id')->all();
    $staffs = App\Entities\Supportstaff::pluck('id')->all();
    $status = array('Resolved', 'Not Resolved');
    return [
        'customer_id' => $faker->randomElement($customers),
        'servicetype_id' => $faker->randomElement($services),
        'branch_id' => $faker->randomElement($branches),
        'supportstaff_id' => $faker->randomElement($staffs),
        'status' => $faker->randomElement($status),
        'description' => $faker->text,
        'complaint_description' => $faker->text,
        'user_id' => $faker->randomElement($users),
        'ptcl_ticket_no' => $faker->randomNumber(),
    ];
});

$factory->define(App\Entities\BranchMedias::class, function (Faker $faker) {
    $branches = App\Entities\Branches::pluck('id')->all();
    return [
        'branch_id' => $faker->randomElement($branches),
        'media_name' => $faker->name,
        'ip' => $faker->ipv4
    ];
});

$factory->define(App\Entities\BranchHardwares::class, function (Faker $faker) {
    $branches = App\Entities\Branches::pluck('id')->all();
    return [
        'branch_id' => $faker->randomElement($branches),
        'hardware_name' => $faker->name,
        'hardware_brand' => $faker->domainName
    ];
});

$factory->define(App\Entities\BranchServiceProvider::class, function (Faker $faker) {
    $branches = App\Entities\Branches::pluck('id')->all();
    return [
        'branch_id' => $faker->randomElement($branches),
        'name' => $faker->name,
        'description' => $faker->text,
        'code' => $faker->randomNumber()
    ];
});