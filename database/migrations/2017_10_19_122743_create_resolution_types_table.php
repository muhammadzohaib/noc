<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResolutionTypesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resolution_types', function(Blueprint $table) {
            $table->increments('id');
			 $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes()->nullable();

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resolution_types');
	}

}
