<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftdeletedInModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch_contacts', function($table) {
            $table->softDeletes()->nullable();
        });

        Schema::table('customers', function($table) {
            $table->softDeletes()->nullable();
        });

        Schema::table('regions', function($table) {
            $table->softDeletes()->nullable();
        });

        Schema::table('servicetypes', function($table) {
            $table->softDeletes()->nullable();
        });

        Schema::table('supportstaffs', function($table) {
            $table->softDeletes()->nullable();
        });

        Schema::table('users', function($table) {
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_contacts', function($table) {
            $table->dropColumn(softDeletes());
        });

        Schema::table('customers', function($table) {
            $table->dropColumn(softDeletes());
        });

        Schema::table('regions', function($table) {
            $table->dropColumn(softDeletes());
        });

        Schema::table('servicetypes', function($table) {
            $table->dropColumn(softDeletes());
        });

        Schema::table('supportstaffs', function($table) {
            $table->dropColumn(softDeletes());
        });

        Schema::table('users', function($table) {
            $table->dropColumn(softDeletes());
        });
    }
}
