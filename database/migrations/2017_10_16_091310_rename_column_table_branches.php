<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnTableBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branches', function($table) {
            $table->renameColumn('`khi tunnel`', 'khi_tunnel');
            $table->renameColumn('`dxx info`', 'dxx_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branches', function($table) {
            $table->renameColumn('khi_tunnel', '`khi tunnel`');
            $table->renameColumn('dxx_info', '`dxx info`');
        });
    }
}
