<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branches', function($table) {
            $table->string('loop_back_ip')->nullable();
            $table->string('it_center')->nullable();
            $table->string('khi tunnel')->nullable();
            $table->string('lhr_tunnel')->nullable();
            $table->string('dxx info')->nullable();
            $table->string('bvpn_1_no')->nullable();
            $table->string('bvpn_1_username')->nullable();
            $table->string('bvpn_1_pw')->nullable();
            $table->string('bvpn_2_no')->nullable();
            $table->string('bvpn_2_username')->nullable();
            $table->string('bvpn_2_pw')->nullable();
            $table->string('evo_esn')->nullable();
            $table->string('evo_mdn')->nullable();
            $table->string('ospf_area')->nullable();
            $table->tinyInteger('branch_power_status')->default('1');
            $table->text('complaint_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branches', function($table) {
            $table->dropColumn('loop_back_ip');
            $table->dropColumn('it_center');
            $table->dropColumn('khi tunnel');
            $table->dropColumn('lhr_tunnel');
            $table->dropColumn('dxx info');
            $table->dropColumn('bvpn_1_no');
            $table->dropColumn('bvpn_1_username');
            $table->dropColumn('bvpn_1_pw');
            $table->dropColumn('bvpn_2_no');
            $table->dropColumn('bvpn_2_username');
            $table->dropColumn('bvpn_2_pw');
            $table->dropColumn('evo_esn');
            $table->dropColumn('evo_mdn');
            $table->dropColumn('ospf_area');
            $table->dropColumn('branch_power_status');
            $table->dropColumn('complaint_description');
        });
    }
}
