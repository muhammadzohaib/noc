<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResolvedByColumnToComplaint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('complaints', function($table) {
            $table->datetime('resolved_at')->nullable();
            $table->integer('resolved_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('complaints', function($table) {
            $table->dropColumn('resolved_at');
            $table->dropColumn('resolved_by');
        });
    }
}
