<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveActiveFromModels extends Migration
{
    /**
     * Run the migration.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('branch_contacts', function($table) {
            $table->dropColumn('active');
        });

        Schema::table('customers', function($table) {
            $table->dropColumn('active');
        });

        Schema::table('regions', function($table) {
            $table->dropColumn('active');
        });

        Schema::table('servicetypes', function($table) {
            $table->dropColumn('active');
        });

        Schema::table('supportstaffs', function($table) {
            $table->dropColumn('active');
        });

        Schema::table('users', function($table) {
            $table->dropColumn('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_contacts', function($table) {
            $table->boolean('active');
        });

        Schema::table('customers', function($table) {
            $table->boolean('active');
        });

        Schema::table('regions', function($table) {
            $table->boolean('active');
        });

        Schema::table('servicetypes', function($table) {
            $table->boolean('active');
        });

        Schema::table('supportstaffs', function($table) {
            $table->boolean('active');
        });

        Schema::table('users', function($table) {
            $table->boolean('active');
        });
    }
}
