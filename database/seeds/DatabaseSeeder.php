<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('complaints')->truncate();
//
//        DB::table('branch_contacts')->truncate();
//        DB::table('branch_medias')->truncate();
//        DB::table('branch_hardwares')->truncate();
//        DB::table('branches')->truncate();
//        DB::table('supportstaffs')->truncate();
//        DB::table('regions')->truncate();
//
//        DB::table('customers')->truncate();
//        DB::table('servicetypes')->truncate();
//
//
//
//        DB::table('role_user')->truncate();


        //DB::table('users')->truncate();
        //DB::table('roles')->truncate();


        $this->call(RolesTableSeeder::class);
        // $this->call(ServiceTypeTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        //  $this->call(RegionsTableSeeder::class);
        // $this->call(BranchesTableSeeder::class);
        // $this->call(BranchContactsTableSeeder::class);
        // $this->call(SupportStaffsTableSeeder::class);

        //  $this->call(ComplaintsTableSeeder::class);
        //  $this->call(BranchMediasTableSeeder::class);
        //  $this->call(BranchHardwaresTableSeeder::class);
        //  $this->call(BranchServiceProviderTableSeeder::class);





    }

}

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'name' => "Manager",
            'email' => 'manager@noc.com',
            'password' => bcrypt('123456'),
        ]);
        factory(App\User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@noc.com',
            'password' => bcrypt('123456'),
        ]);
        factory(App\User::class)->create([
            'name' => 'User',
            'email' => 'user@noc.com',
            'password' => bcrypt('123456'),
        ]);


        factory(App\Role::class)->create([
            'name' => "manager"
        ]);
        factory(App\Role::class)->create([
            'name' => "admin"
        ]);
        factory(App\Role::class)->create([
            'name' => "user"
        ]);


        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);
        DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 2
        ]);
        DB::table('role_user')->insert([
            'user_id' => 3,
            'role_id' => 3
        ]);
    }
}

class ServiceTypeTableSeeder extends Seeder
{

    public function run()
    {

        factory(App\Entities\Servicetypes::class, 50)->create();
    }
}

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Entities\Customers::class)->create([
            'alias' => 'NBP',
            'name' => 'National Bank Of Pakistan'
        ]);
    }
}

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entities\Regions::class, 3)->create();

    }
}

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entities\Branches::class, 500)->create();


    }

}

class BranchContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entities\BranchContacts::class, 50)->create();



    }

}


class SupportStaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Entities\Supportstaff::class, 50)->create();


    }

}

class ComplaintsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Entities\Complaint::class, 500)->create();



    }

}

class BranchMediasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Entities\BranchMedias::class, 50)->create();


    }

}

class BranchHardwaresTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Entities\BranchHardwares::class, 50)->create();



    }

}

class BranchServiceProviderTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Entities\BranchServiceProvider::class, 500)->create();


    }

}