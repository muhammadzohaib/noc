<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('auth/login');
//});

Route::get('/', ['middleware' => ['checkrole'], function() {
    return view('auth/login');
}]);

// Protected Routes: For All Roles
Route::middleware(['App\Http\Middleware\ProtectedRoutes'])->group(function () {
    Route::get('/changepassword', 'UsersController@changepassword')->name('changepassword');
    Route::post('updatepassword', 'UsersController@updatepassword')->name('updatepassword');

//    all tickets i.e. resolved and unresolved
    Route::resource('complaints', ComplaintController::class);
    Route::get('/getcomplaints', 'ComplaintController@getcomplaints')->name('getcomplaints');
    Route::get('/getBranchesByCustomer', 'BranchesController@getBranchesByCustomer')->name('getbranchesbycustomer');
    //    unresolved tickets
    Route::get('/unresolvedtickets', 'ComplaintController@unresolvedticket');
    Route::get('/getunresolvedtickets', 'ComplaintController@getunresolvedtickets')->name('getunresolvedtickets');
    //    resolved tickets
    Route::get('/resolvedtickets', 'ComplaintController@resolvedindex');
    Route::get('/getresolvedtickets', 'ComplaintController@getresolvedtickets')->name('getresolvedtickets');;

});

Route::post('managerdashboard', 'ManagersController@index')->name('managerdashboard');

// role: admin -- routes
Route::middleware(['App\Http\Middleware\AdminRoutes'])->group(function () {
    Route::resource('admins', AdminController::class);
});

// role: manager -- routes
Route::middleware(['App\Http\Middleware\ManagerRoutes'])->group(function () {
    Route::resource('managers', ManagersController::class, ['only' => ['index']]);
});

// role: user -- routes
Route::middleware(['App\Http\Middleware\UserRoutes'])->group(function () {
    Route::resource('users', UsersController::class, ['only' => ['index']]);
});

//protected routes -- user role can't access these routes
Route::middleware(['App\Http\Middleware\RouteAuth'])->group(function () {
    Route::resource('customers', CustomersController::class);
    Route::resource('regions', RegionsController::class);
    Route::get('/userslist', 'UsersController@Userslist')->name('userslist');
    Route::get('/getuserslist', 'UsersController@getuserslist')->name('getuserslist');
    Route::resource('managers', ManagersController::class, ['except' => ['index']]);
    Route::resource('servicetypes', ServiceTypeController::class);
    Route::resource('resolutiontypes', ResolutionTypesController::class);
    Route::resource('supportstaff', SupportStaffController::class);
    Route::resource('branches', BranchesController::class);
    Route::get('/getbranches', 'BranchesController@getbranches')->name('getbranches');
    Route::resource('branchcontacts', Branch_ContactsController::class);
    Route::get('/getbranchcontacts', 'Branch_ContactsController@getbranchcontacts')->name('getbranchcontacts');
    Route::resource('branchhardwares', Branch_HardwaresController::class);
    Route::get('/getbranchhardwares', 'Branch_HardwaresController@getbranchhardwares')->name('getbranchhardwares');
    Route::resource('branchmedias', Branch_MediasController::class);
    Route::get('/getbranchmedias', 'Branch_MediasController@getbranchmedias')->name('getbranchmedias');
    Route::resource('branchserviceproviders',Branch_ServiceProvidersController::class);
    Route::get('/getserviceproviders', 'Branch_ServiceProvidersController@getserviceproviders')->name('getserviceproviders');
    Route::resource('users', UsersController::class, ['except' => ['index']]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');